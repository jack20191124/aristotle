[ ![Download](https://api.bintray.com/packages/jack20191124/Maven/Aristotle/images/download.svg) ](https://bintray.com/jack20191124/Maven/Aristotle/_latestVersion) [ ![Build Status](https://img.shields.io/badge/build-passing-brightgreen.svg) ](https://jack20191124.bitbucket.io/sites/aristotle/surefire-report.html) [ ![Code Coverage](https://img.shields.io/badge/coverage-65%25-brightgreen.svg) ](https://jack20191124.bitbucket.io/sites/aristotle/jacoco/index.html) [ ![JavaDoc](https://img.shields.io/badge/JavaDoc-1.0-blueviolet.svg) ](https://jack20191124.bitbucket.io/sites/aristotle/apidocs/index.html) [ ![License Badge](https://img.shields.io/badge/License-Apache%202.0-orange.svg) ](https://www.apache.org/licenses/LICENSE-2.0)

Aristotle
=========

**"κακὰ συνάγει τοὺς ἀνθρώπους"**

Aristotle is a [collection of sharable Java API's](https://jack20191124.bitbucket.io/Aristotle/site/apidocs/index.html):

* [Programmatically Construct GraphQL Query](https://jack20191124.bitbucket.io/Aristotle/site/apidocs/org/bitbucket/jack20191124/graphql/query/package-summary.html)
* [Programmatically Construct GraphQL Response](https://jack20191124.bitbucket.io/Aristotle/site/apidocs/org/bitbucket/jack20191124/graphql/test/relayjsonapi/package-summary.html)
* JAX-RS
    - [Standup a Jersey-Jetty server for integration tests](https://jack20191124.bitbucket.io/Aristotle/site/apidocs/org/bitbucket/jack20191124/jaxrs/test/JettyServerFactory.html)
* [Concurrency](https://jack20191124.bitbucket.io/Aristotle/site/apidocs/org/bitbucket/jack20191124/concurrency/package-summary.html)
* [Caching](https://jack20191124.bitbucket.io/Aristotle/site/apidocs/org/bitbucket/jack20191124/caching/package-summary.html)
* [Useful Annotations](https://jack20191124.bitbucket.io/Aristotle/site/apidocs/org/bitbucket/jack20191124/annotation/package-summary.html)
* [I/O Utilities](https://jack20191124.bitbucket.io/Aristotle/site/apidocs/org/bitbucket/jack20191124/io/IOUtils.html)
* JSON
    - [Opinionated Jackson Mapper with common configuration](https://jack20191124.bitbucket.io/Aristotle/site/apidocs/org/bitbucket/jack20191124/json/ObjectMapperFactory.html)

**Note that [Aristotle](https://bitbucket.org/jack20191124/aristotle) assumes absolutely no support for
[Java Object Serialization Specification](https://docs.oracle.com/javase/8/docs/platform/serialization/spec/serialTOC.html)
due to its
[possible removal of the spec in the future](https://adtmag.com/articles/2018/05/30/java-serialization.aspx)**.

## Package Design Principles and Philosophy

[Aristotle](https://bitbucket.org/jack20191124/aristotle) rose from the idea of
[Layers Architectural Pattern](_Pattern-Oriented Software Architecture Vol. 1_, 1996, Ch.2, 2.2) with **the mission of
supporting coding standardization**, i.e. [Aristotle](https://bitbucket.org/jack20191124/aristotle) offers clearly-defined
and commonly-accepted levels of abstraction, which enables the development of standardized tasks and interfaces.
Different implementations of the same interface can be used interchangeably.

[Aristotle](https://bitbucket.org/jack20191124/aristotle) also provides a well-defined abstraction and has a well-defined
and documented interface. Hence it can be reused in multiple contexts.

A strong driving force for elaborating and creating [Aristotle](https://bitbucket.org/jack20191124/aristotle) was to
balance two contradictory goals for Java API design: **immutability** and **performance**. In the ideal case, an API
would consists of all immutable objects for thread safety and would also run pretty fast with small memory footprint. In
the real world, a trade-off between these goals has to be found. To address this trade-off,
[Aristotle](https://bitbucket.org/jack20191124/aristotle) **honors immutability first**:

### Immutability

* A type is always made to be immutable by all efforts unless immutability affects the correctness of API
* All object instances are statically loaded to promote thread-safety unless it cannot
* Whenever possible, API objects are instantiated as singleton via static factory method (_Effective Java_, 2nd Ed.
  Item 1) to reduce memory footprint
  
### Performance

* Since [Aristotle](https://bitbucket.org/jack20191124/aristotle) is baked out of the idea of layers architectural pattern,
  it inherits one of the liabilities of layers architectural pattern: a layered architecture is usually less efficient
  than a monolithic structure or a 'sea of objects'
* A monolithic structure does not tie to any standard due to performance needs. In the case of
  squeezing-last-piece-of-performance type application which performs best case-by-case, such universal API does not
  exist for multiple performance-critical applications

## Binaries - How to Get Aristotle

Binaries are stored in Bintray. Dependency information for Maven, Ivy, and Gradle can be found at
https://bintray.com/jack20191124/Maven/Aristotle.

### Maven Example

#### Step 1 - POM

```xml
<dependency>
    <groupId>org.bitbucket.jack20191124org.bitbucket.jack20191124</groupId>
    <artifactId>Aristotle</artifactId>
    <version>x.y</version>
</dependency>
```

#### Step 2 - settings.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<settings xsi:schemaLocation='http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd'
          xmlns='http://maven.apache.org/SETTINGS/1.0.0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
    
    <profiles>
        <profile>
            <repositories>
                <repository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>bintray-jack20191124-Maven</id>
                    <name>bintray</name>
                    <url>https://dl.bintray.com/jack20191124/Maven</url>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>bintray-jack20191124-Maven</id>
                    <name>bintray-plugins</name>
                    <url>https://dl.bintray.com/jack20191124/Maven</url>
                </pluginRepository>
            </pluginRepositories>
            <id>bintray</id>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>bintray</activeProfile>
    </activeProfiles>
</settings>
```

## Comparision with Other Packages

This is not really a comparison, but more a hinting to other possibilities for general Java API. I am not aware of
another package which implements some of the functions as defined in
[Aristotle](https://bitbucket.org/jack20191124/aristotle), but surely there are excellent other general-purpose API's.
The first to name is [Apache Commons](http://commons.apache.org/). One could find a bunch of good API's with different
algorithms. The second one is Google Guava which is also very powerful.

## For Windows Developers

If you see the similar error below while running maven tests

```
Exception in thread "main" java.lang.UnsatisfiedLinkError: org.apache.hadoop.io.nativeio.NativeIO$Windows.access0(Ljava/lang/String;I)Z
```

This usually means you don't have Hadoop installed properly on Windows. Please download [it](https://github.com/steveloughran/winutils/tree/master/hadoop-3.0.0/bin)
and [install by setting the HADOOP_HOME & PATH environment variables](https://mkyong.com/maven/how-to-install-maven-in-windows/)

## License

The use and distribution terms for this software are covered by the Apache License, Version 2.0
( http://www.apache.org/licenses/LICENSE-2.0.html ).
