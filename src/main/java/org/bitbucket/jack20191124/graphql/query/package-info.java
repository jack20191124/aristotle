/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The general principle is to use as many types/classes as possible to make the code easy to read and maintain.
 * Although fewer types/classes/concepts gives shorter code, combining too much into a single class will make the
 * codebase very hard to read and maintain.
 * <p>
 * Using this package requires solid understanding of GraphQL query language grammer, i.e. ANTLR4.
 * <p>
 * @see <a href="https://github.com/graphql-java/graphql-java/tree/master/src/main/antlr">ANTLR4 Definition</a>
 */
package org.bitbucket.jack20191124.graphql.query;
