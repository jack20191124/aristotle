/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.map;

import org.bitbucket.jack20191124.StreamUtils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.NotNull;

/**
 * A delegating map has a local copy(self) of keys and values and a next map to which it delegates lookups for values
 * which it doesn't contain. Operations which remove keys from a map throw exceptions because the local map cannot alter
 * its delegate (removing a key with {@link java.util.LinkedHashMap#remove(Object)} from the map and then still having
 * {@link java.util.LinkedHashMap#get(Object)} return the value from the delegate would be a very surprising behavior on
 * Map).
 * <p>
 * In order to support predictable iteration ordering, this extends {@link java.util.LinkedHashMap} with a modified
 * definition of canonical ordering based on the canonical ordering of non-shadowed keys in the delegate followed by
 * local keys in insertion order (e.g. {@link java.util.LinkedHashMap}).
 *
 * @param <K>  The key type for the map
 * @param <V>  The value type for the map
 */
public class DelegatingMap<K, V> extends LinkedHashMap<K, V> {
    /**
     * {@link java.util.Map} to delegate to.
     */
    private final Map<K, V> delegate;

    /**
     * Constructor.
     *
     * @param nextMap  {@link java.util.Map} to delegate to
     */
    public DelegatingMap(@NotNull final Map<K, V> nextMap) {
        delegate = nextMap;
    }

    /**
     * Constructor.
     * <p>
     * Uses a {@link java.util.LinkedHashMap} as it's base {@link java.util.Map}.
     */
    public DelegatingMap() {
        this(new LinkedHashMap<>());
    }

    @Override
    public V get(final Object key) {
        return super.containsKey(key) ? super.get(key) : delegate.get(key);
    }

    /**
     * Removes are not allowed: Delegating Map should be changed in a put-only fashion.
     */
    @Override
    public void clear() {
        throw new UnsupportedOperationException(
                "Clear is not defined on DelegatingMap. Add is the only allowed modification operation."
        );
    }

    /**
     * Clears the local map. This won't affect the delegate map.
     */
    public void clearLocal() {
        super.clear();
    }

    @Override
    public boolean containsKey(final Object key) {
        return super.containsKey(key) || delegate.containsKey(key);
    }

    @Override
    public boolean containsValue(final Object value) {
        return entryStream().map(Map.Entry::getValue).anyMatch(v -> Objects.equals(v, value));
    }

    @Override
    public Set<K> keySet() {
        return entryStream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        return entryStream().collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Adds the key and value to the local map, potentially overshadowing a version in the delegate map.
     *
     * @param key  The key to the value
     * @param value  The value being stored
     *
     * @return the prior value from the local map (if any) or the shadowed value from the delegate (if any)
     */
    @Override
    public V put(final K key, final V value) {
        final V oldValue = super.put(key, value);
        return (oldValue == null) ? delegate.get(key) : oldValue;
    }

    /**
     * Delegating map should be changed in a put-only fashion. Removes are not allowed.
     *
     * @param object  Object to be removed
     *
     * @return nothing, this is not supported and throws an exception instead
     */
    @Override
    public V remove(final Object object) {
        throw new UnsupportedOperationException(
                "Remove is not defined on DelegatingMap. Add is the only allowed modification operation."
        );
    }

    /**
     * Remove from the local map.
     *
     * @param key  Key whose mapping is to be removed from the local map
     *
     * @return the previous value associated with key, or null if there was no mapping for key
     */
    public V removeLocal(final K key) {
        return super.remove(key);
    }

    @Override
    public int size() {
        return entrySet().size();
    }

    @Override
    public Collection<V> values() {
        return entryStream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toSet());
    }

    /**
     * Returns a non-delegating map which snapshots the data visible in this delegating map, disconnected from changes
     * to this and the underlying delegates.
     *
     * @return a non-delegating plan-map copy of the data visible in this map
     */
    public LinkedHashMap<K, V> flatView() {
        return entryStream()
                .collect(StreamUtils.toLinkedMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), delegate);
    }

    /**
     * Returns true if the two delegating maps are the same in terms of the current layer and their delegates are equal
     * under the definition of equality for that delegate.
     *
     * @param other  The object being compared
     *
     * @return true if the object being compared has the same local values and their delegates are equal
     */
    @Override
    public boolean equals(final Object other) {
        if (!(other instanceof DelegatingMap)) {
            return false;
        }
        final DelegatingMap that = (DelegatingMap) other;
        return super.equals(that) && delegate.equals(that.delegate);
    }

    /**
     * Returns a {@link java.util.stream.Stream stream} of map entries.
     *
     * @return a {@link java.util.stream.Stream stream} of entries which are visible from via delegate and those in the
     * local map
     */
    private Stream<Map.Entry<K, V>> entryStream() {
        return Stream.concat(
                delegate.entrySet().stream().filter(entry -> !super.containsKey(entry.getKey())),
                super.entrySet().stream()
        );
    }
}
