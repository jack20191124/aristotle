/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.jcip.annotations.Immutable;
import net.jcip.annotations.ThreadSafe;

/**
 * {@link ObjectMapperFactory} is the provider of all Jackson mappers that are used for object
 * serialization/deserialization.
 * <p>
 * {@link ObjectMapperFactory} keeps all mappers as singletons.
 */
@Immutable
@ThreadSafe
public class ObjectMapperFactory {

    /**
     * Singleton {@link ObjectMapper}.
     */
    private static final ObjectMapper INSTANCE = initMapper();

    /**
     * Constructor.
     * <p>
     * Suppress default constructor for noninstantiability.
     */
    private ObjectMapperFactory() {
        throw new AssertionError();
    }

    /**
     * Returns a singleton {@link ObjectMapper}.
     *
     * @return the same {@link ObjectMapper} instance
     */
    public static ObjectMapper getInstance() {
        return INSTANCE;
    }

    /**
     * Returns a initialized {@link ObjectMapper}.
     *
     * @return a new {@link ObjectMapper}
     */
    private static ObjectMapper initMapper() {
        return new ObjectMapper();
    }
}
