/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.table;

import org.bitbucket.jack20191124.CollectionUtils;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * An interface describing a table or table-like entity composed of sets of columns.
 */
public interface Schema {

    /**
     * Returns all columns underlying this Schema.
     *
     * @return all columns of this Schema
     */
    Set<Column> getColumns();

    /**
     * Returns set of columns by sub-type.
     *
     * @param columnClass the class of columns to search
     * @param <T> sub-type of Column to return
     *
     * @return set of Columns by the sub-type
     *
     * @throws NullPointerException if the sub-type to search is {@code null}
     */
    default <T extends Column> LinkedHashSet<T> getColumns(final Class<T> columnClass) {
        return CollectionUtils.getSubsetByType(getColumns(), Objects.requireNonNull(columnClass));
    }

    /**
     * Given a column type and a name, returns an optional of column of the expected type and name.
     * <p>
     *
     * @param name the column name
     * @param columnClass the class of the column being retrieved
     * @param <T> the type of the sub-class of the column being retrieved
     *
     * @return the optional containing the column of the name and type specified
     *
     * @throws NullPointerException if the column name or sub-class type is {@code null}
     */
    default <T extends Column> Optional<T> getColumn(final String name, final Class<T> columnClass) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(columnClass);
        return getColumns(columnClass).stream()
                .filter(column -> column.getName().equals(name))
                .findFirst();
    }
}
