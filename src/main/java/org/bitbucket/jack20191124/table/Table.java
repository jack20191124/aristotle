/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.table;

/**
 * An interface for table or table-like entity with schema.
 */
public interface Table {

    /**
     * Returns the schema for this table.
     *
     * @return the schema for this table.
     */
    Schema getSchema();

    /**
     * Returns the name of this table.
     *
     * @return the table name
     */
    TableName getName();

}
