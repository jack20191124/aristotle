/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.table;

/**
 * Column.
 */
public class Column {

    /**
     * String used for {@code toString}.
     */
    private static final String TO_STRING_FORMAT = "Column{name='%s'}";

    /**
     * Column name.
     */
    private final String name;

    /**
     * Constructs a new {@code Column} with the specified column name.
     *
     * @param name column name
     */
    public Column(final String name) {
        this.name = name;
    }

    /**
     * Returns the column name.
     *
     * @return the column name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the string representation of this column.
     * <p>
     * The format of the string is "Column{name='XXX'}", where XXX is given by {@link #getName()}. Note that the value
     * is surrounded by a pair of single quotes.
     *
     * @return the string representation of this column.
     */
    @Override
    public String toString() {
        return String.format(TO_STRING_FORMAT, getName());
    }
}
