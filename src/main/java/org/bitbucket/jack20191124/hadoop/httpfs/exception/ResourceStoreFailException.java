/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.hadoop.httpfs.exception;

/**
 * Exception during resource storage to HDFS via HttpFS server.
 */
public class ResourceStoreFailException extends HttpFsException {

    /**
     * Constructors a new {@code ResourceStoreFailException} with specified detail message.
     * <p>
     * The cause is not initialized, and may subsequently be initialized by a call to {@link #initCause(Throwable)}.
     *
     * @param message  the detail message. The detail message is saved for later retrieval by the {@link #getMessage()}
     * method.
     */
    public ResourceStoreFailException(final String message) {
        super(message);
    }

    /**
     * Constructors a new {@code ResourceStoreFailException} with specified detail message and cause.
     * <p>
     * Note that the detail message associated with {@code cause} is <i>not</i> automatically incorporated in this
     * runtime exception's detail message.
     *
     * @param message the detail message (which is saved for later retrieval by the {@link #getMessage()} method)
     * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt>
     * value is permitted, and indicates that the cause is nonexistent or unknown.)
     */
    public ResourceStoreFailException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
