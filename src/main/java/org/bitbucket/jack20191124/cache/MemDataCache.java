/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.cache;

import com.google.common.base.Preconditions;

import net.jcip.annotations.ThreadSafe;
import net.spy.memcached.MemcachedClient;

import java.util.Objects;
import java.util.concurrent.ExecutionException;

/**
 * Memcached client implementation of {@link DataCache}.
 *
 * @param <V> type of data
 */
@ThreadSafe
public class MemDataCache<V> implements DataCache<String, V> {

    /**
     * The Memcached client to support this cache.
     */
    private final MemcachedClient memcachedClient;

    /**
     * The expiration of cache.
     */
    private final int expiration;

    /**
     * Constructs a new {@code MemDataCache} with the specified supporting Memcached client and expiration of cache.
     *
     * @param memcachedClient the Memcached client to support this cache
     * @param expiration the expiration of cache is not a positive number
     *
     * @throws NullPointerException if the specified client is {@code null}
     * @throws IllegalArgumentException is the specified expiration is not positive
     */
    public MemDataCache(final MemcachedClient memcachedClient, final int expiration) {
        Objects.requireNonNull(memcachedClient);
        Preconditions.checkArgument(expiration > 0, "Cache expiration must be positive.");
        this.memcachedClient = memcachedClient;
        this.expiration = expiration;
    }

    /**
     * Reads and returns data from cache by key.
     * <p>
     * If no cache exists for the key, returns {@code null}
     *
     * @param key  the key whose associated value is to be returned
     *
     * @return the value to which the specified key is cached, or {@code null} if cache contains no mapping for the key
     *
     * @throws NullPointerException if the key is {@code null}
     */
    @SuppressWarnings("unchecked")
    @Override
    public V get(final String key) {
        Objects.requireNonNull(key);
        return (V) memcachedClient.get(key);
    }

    /**
     * Put a value on a key in a data cache.
     *
     * @param key the key under which this object should be added.
     * @param value the object to store
     *
     * @return a boolean representing success of thi operation
     *
     * @throws NullPointerException if the key or value is {@code null}
     * @throws IllegalStateException if the set failed
     */
    @Override
    public boolean set(final String key, final V value) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(value);
        try {
            return memcachedClient.set(key, getExpiration(), value).get();
        } catch (final InterruptedException | ExecutionException exception) {
            throw new IllegalStateException(exception);
        }
    }

    @Override
    public void clear() {
        memcachedClient.flush();
    }

    /**
     * Returns the expiration of cache.
     *
     * @return the expireatin of cache
     */
    public int getExpiration() {
        return expiration;
    }
}
