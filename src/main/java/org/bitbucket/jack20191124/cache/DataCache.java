/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.cache;

/**
 * {@link DataCache} represents a common key-value cache interface for data applications.
 * <p>
 * Implementations can be in-memory or on top of a remote source such as Redis. It is implementor's choice to decide
 * whether or not the cache supports {@code null} key or {@code null} values.
 *
 * @param <K>  The key type being cached
 * @param <V>  The value type being cached
 */
public interface DataCache<K, V> {

    /**
     * Reads and returns data from cache by key.
     *
     * @param key  The key whose associated value is to be returned
     *
     * @return an object representing or containing the the value to which the specified key is cached
     */
    V get(K key);

    /**
     * Puts a value on a key in a data cache.
     *
     * @param key  The key under which this object should be added.
     * @param value  The object to store
     *
     * @return a boolean representing success of the operation
     */
    boolean set(K key, V value);

    /**
     * Removes all of the mappings from this cache.
     */
    void clear();
}
