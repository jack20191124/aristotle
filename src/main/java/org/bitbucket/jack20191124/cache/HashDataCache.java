/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.cache;

import org.bitbucket.jack20191124.annotation.NotNull;

import net.jcip.annotations.NotThreadSafe;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.AbstractMap;
import java.util.Base64;
import java.util.Objects;

/**
 * {@link HashDataCache} is designed for key-value pair cache whose original key is very long; it uses hashed keys to
 * reduce the original key length for the provided underlying cache.
 * <p>
 * For example, if user needs to cache a key-value pair of {@code longKey -> value}), this pair is going to be stored in
 * this cache as {@code hash(longKey) -> pair(longKey, value)}.
 * <p>
 * {@link HashDataCache} is best for cache backed by Memcached, which requires its cache key to be no more than 250
 * bytes. For example, if the cache key is a query string(which can potentially be very long), the query string can be
 * hashed to 128 bytes long. This hash will be the key of {@link HashDataCache}, and both original query and query
 * results will be stored as value of {@link HashDataCache}
 * <p>
 * Here is a concrete example use case of {@link HashDataCache}: when we cache database response in Memcached, the
 * most straight forward strategy is to use database query as cache key and database result as cache value. The big
 * issue with this approach, however, is that Memcached requires all cache key to be less than 250 bytes. Since some
 * database query can be much longer than 250 bytes long (take a look at Druid's JSON query for example), the
 * solution is to hash all database query to a fixed length, such as 128 bytes, use this fixed hash as cache key, and
 * the original query and database result as the cache value. {@link HashDataCache} offers this solution out of the box.
 * <p>
 * {@link HashDataCache} assumes both original key and hashed key to be of type {@link String}. The cache value,
 * however, is parameterized. In Javadoc of {@link HashDataCache}, we refer "original key" as the aformentioned query
 * string, "hash key" as the hash of the query string, and "cache value" as the aformentioned query result.
 * <p>
 * Since key needs to be hashed, {@link HashDataCache} does not allow {@code null} key but does allow {@code null} value
 *
 * @param <V>  The value type being cached
 */
@NotThreadSafe
public class HashDataCache<V> implements DataCache<String, V> {

    /**
     * A key/value pair that together represents a cached value.
     *
     * @param <V> type of the value in the pair
     */
    public static class Pair<V> extends AbstractMap.SimpleImmutableEntry<String, V> {

        /**
         * All-args constructor.
         *
         * @param key  The original cache key
         * @param value  The cache value
         */
        private Pair(final String key, final V value) {
            super(key, value);
        }
    }

    private final DataCache<String, Pair<V>> cache;
    private final MessageDigest messageDigest;

    /**
     * Constructs a new {@code HashDataCache} with the provided underlying cache and name of algorithm which is used to
     * hash cache keys.
     *
     * @param cache  An underlying cache whose key is a hashed and value is a pair of unhashed key and cache value
     * @param algorithm  The specified MessageDigest hash algorithm name
     *
     * @throws NullPointerException if the provided underlying cache or algorithm name is {@code null}
     * @throws NoSuchAlgorithmException if there is no MessageDigest implementation for the specified algorithm
     */
    public HashDataCache(@NotNull final DataCache<String, Pair<V>> cache, @NotNull final String algorithm)
            throws NoSuchAlgorithmException {
        this(cache, MessageDigest.getInstance(Objects.requireNonNull(algorithm, "algorithm")));
    }

    /**
     * All-args constructor.
     *
     * @param cache  An underlying cache whose key is a hashed and value is a pair of unhashed key and cache value
     * @param messageDigest  A hash function that takes arbitrary-sized key and output a fixed-length hash key
     *
     * @throws NullPointerException if any argument is {@code null}
     */
    private HashDataCache(@NotNull final DataCache<String, Pair<V>> cache, @NotNull final MessageDigest messageDigest) {
        this.cache = Objects.requireNonNull(cache, "cache");
        this.messageDigest = Objects.requireNonNull(messageDigest, "messageDigest");
    }

    /**
     * Reads and returns data from cache by key.
     * <p>
     * If no cache exists for the key or key hash collision happens, returns {@code null}
     *
     * @param key  the key whose associated value is to be returned
     *
     * @return the value to which the specified key is cached, or {@code null} if cache contains no mapping for the key
     * or key hash collision happens
     *
     * @throws NullPointerException if the cache key is {@code null}
     */
    @Override
    @SuppressWarnings("MultipleStringLiterals")
    public V get(@NotNull final String key) {
        Objects.requireNonNull(key, "key");

        final String hashKey = hash(getMessageDigest(), key);
        final Pair<V> pair = getCache().get(hashKey);
        if (pair == null) {
            return null;
        } else if (Objects.equals(key, pair.getKey())) {
            return pair.getValue();
        } else {
            return null; // hash collision
        }
    }

    @Override
    @SuppressWarnings("MultipleStringLiterals")
    public boolean set(@NotNull final String key, final V value) {
        Objects.requireNonNull(key, "key");
        return getCache().set(hash(getMessageDigest(), key), new Pair<>(key, value));
    }

    @Override
    public void clear() {
        getCache().clear();
    }

    /**
     * Returns the Base64 hash encoding of a text string.
     * <p>
     * Note that this method is not thread-safe and could be the performance bottle-neck, because {@link MessageDigest}
     * spins up locking mechanism while hashing data
     *
     * @param messageDigest a hash function that take arbitrary-length text and output a fixed-length hash value.
     * @param text the input string to encode
     *
     * @return the hash encoding of the text as string
     *
     * @throws NullPointerException if the message digest or text is {@code null}.
     */
    @NotNull
    private static String hash(@NotNull final MessageDigest messageDigest, @NotNull final String text) {
        Objects.requireNonNull(text);

        final byte[] keyBytes = text.getBytes(StandardCharsets.UTF_8);
        messageDigest.update(keyBytes, 0, keyBytes.length);
        return Base64.getEncoder().encodeToString(messageDigest.digest());
    }

    @NotNull
    private DataCache<String, Pair<V>> getCache() {
        return cache;
    }

    @NotNull
    private MessageDigest getMessageDigest() {
        return messageDigest;
    }
}
