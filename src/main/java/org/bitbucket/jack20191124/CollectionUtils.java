/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

/**
 * Collection-type object utils.
 */
public class CollectionUtils {

    /**
     * Given a collection of objects with which share the same super class, returns the subset of objects that share a
     * common sub-class.
     *
     * @param set  The input collection of objects that share the same super class
     * @param type  The sub-class type
     *
     * @param <T>  Super class type
     * @param <K>  Sub class type
     *
     * @return ordered subset of objects that share a common sub-class
     */
    public static <T, K extends T> LinkedHashSet<K> getSubsetByType(final Collection<T> set, final Class<K> type) {
        return set.stream()
                .filter(member -> type.isAssignableFrom(member.getClass()))
                .map(StreamUtils.uncheckedCast(type))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Turn instances of an object into a LinkedHashSet.
     *
     * @param e  The array from which the LinkedHashSet will be built
     * @param <E>  The element type for the LinkedHashSet
     *
     * @return a LinkedHashSet view of the specified array.
     */
    @SafeVarargs
    public static <E> LinkedHashSet<E> asLinkedHashSet(final E... e) {
        return new LinkedHashSet<>(Arrays.asList(e));
    }
}
