/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.query.searchqueryspec;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Locale;

/**
 * Enum for specifying the type of search query spec.
 */
public enum DefaultSearchQuerySpecType implements SearchQuerySpecType {

    /**
     * dfadfads.
     */
    CONTAINS,

    /**
     * See {@link FragmentSearchQuerySpec}.
     */
    FRAGMENT,

    /**
     * dfasdfds.
     */
    INSENSITIVE_CONTAINS,

    /**
     * dfasdf.
     */
    REGEX;

    /**
     * The lower case name for the value of {@code type} attribute in search query spec.
     */
    private final String lowerCaseName;

    /**
     * Constructs for one of the values of {@code DefaultSearchQuerySpecType}.
     */
    DefaultSearchQuerySpecType() {
        this.lowerCaseName = name().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Returns a serialized JSON representation of this search query spec.
     * <p>
     * The string is the lower case version of the enum value.
     *
     * @return the serialized JSON representation of this search query spec
     */
    @JsonValue
    @Override
    public String toString() {
        return lowerCaseName;
    }
}
