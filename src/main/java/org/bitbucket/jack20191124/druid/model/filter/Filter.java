/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.filter;

import com.fasterxml.jackson.annotation.JsonValue;

import org.bitbucket.jack20191124.EnumUtils;

import net.jcip.annotations.Immutable;

import java.util.Objects;

/**
 * Model representing a filter expression in a Druid query.
 * <p>
 * A filter is a JSON object indicating which rows of data should be included in the computation for a query. It is
 * essentially the equivalent of the WHERE clause in SQL.
 */
@SuppressWarnings("AbstractClassName")
public abstract class Filter {

    /**
     * Valid types for Druid filters.
     */
    @Immutable
    public enum DefaultFilterType implements FilterType {

        /**
         * See {@link SelectorFilter}.
         */
        SELECTOR,

        /**
         * TBD.
         */
        REGEX,

        /**
         * TBD.
         */
        AND,

        /**
         * TBD.
         */
        OR,

        /**
         * TBD.
         */
        NOT,

        /**
         * TBD.
         */
        EXTRACTION,

        /**
         * TBD.
         */
        SEARCH,

        /**
         * TBD.
         */
        IN;

        final String jsonName;

        /**
         * Constructs a {@code DefaultFilterType} from a specified filter type enum.
         */
        DefaultFilterType() {
            this.jsonName = EnumUtils.enumJsonName(this);
        }

        /**
         * Returns the JSON representation of this class.
         *
         * @return the JSON representation of this class
         */
        @JsonValue
        public String toJson() {
            return jsonName;
        }

        @Override
        public String toString() {
            return toJson();
        }
    }

    /**
     * Type of the filter.
     */
    private final FilterType type;

    /**
     * Constructs a new {@code Filter} with the specified filter type.
     *
     * @param type type of the filter
     *
     * @throws NullPointerException if the filter type is {@code null}.
     */
    protected Filter(final FilterType type) {
        this.type = Objects.requireNonNull(type, "filter type");
    }

    /**
     * Returns the type of this filter.
     *
     * @return the type of this filter
     */
    public FilterType getType() {
        return type;
    }
}
