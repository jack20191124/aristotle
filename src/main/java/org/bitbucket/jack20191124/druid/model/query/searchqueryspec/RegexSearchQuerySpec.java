/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.query.searchqueryspec;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.regex.Pattern;

/**
 * Class for specifying the RegexSearchQuerySpec.
 * <p>
 * If any part of a dimension value contains the pattern specified in this search query spec, a "match" occurs. The
 * grammar is:
 * <pre>
 * {
 *     "type": "regex",
 *     "value": "some_pattern"
 * }
 * </pre>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "pattern" })
public class RegexSearchQuerySpec extends AbstractSearchQuerySpec {

    /**
     * Pattern for the regex to match.
     */
    private final Pattern pattern;

    /**
     * Constructs a new {@code RegexSearchQuerySpec} with the specified regex pattern.
     * <p>
     * The regex pattern can be {@code null}.
     *
     * @param pattern pattern for the regex to match
     */
    public RegexSearchQuerySpec(final Pattern pattern) {
        super(DefaultSearchQuerySpecType.REGEX);
        this.pattern = pattern;
    }

    /**
     * Returns the pattern for the regex to match in this search query spec.
     *
     * @return the search pattern
     */
    public Pattern getPattern() {
        return pattern;
    }

    /**
     * Returns the string representation of this search query spec.
     * <p>
     * <b>The string is NOT the JSON representation used for Druid query.</b> The format of the string is
     * "RegexSearchQuerySpec{type=XXX, pattern=YYY}", where XXX is given by {@link #getType()} and YYY by
     * {@link #getPattern()}. Note that the two values are separated by a comma followed by a single space.
     *
     * @return the string representation of this search query spec
     */
    @Override
    public String toString() {
        return String.format("RegexSearchQuerySpec{type=%s, pattern=%s}", getType(), getPattern());
    }
}
