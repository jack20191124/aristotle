/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.granularity;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;

/**
 * Duration granularities are specified as an exact duration in milliseconds and timestamps are returned as UTC.
 * <p>
 * Duration granularity values are in millis. {@code DurationGranularity} also supports specifying an optional origin,
 * which defines where to start counting time buckets from (defaults to 1970-01-01T00:00:000Z).
 * <p>
 * For example, this chunks up every 2 hours:
 * <pre>
 * {"type": "duration", "duration": 7200000}
 * </pre>
 * This chunks up every 1 hour on the half-hour:
 * <pre>
 * {"type": "duration", "duration": 3600000, "origin": "2012-01-01T00:30:00Z"}
 * </pre>
 * Suppose you have data below stored in Druid with millisecond ingestion granularity
 * <pre>
 *     {"timestamp": "2013-08-31T01:02:33Z", "page": "AAA", "language" : "en"}
 *     {"timestamp": "2013-09-01T01:02:33Z", "page": "BBB", "language" : "en"}
 *     {"timestamp": "2013-09-02T23:32:45Z", "page": "CCC", "language" : "en"}
 *     {"timestamp": "2013-09-03T03:32:45Z", "page": "DDD", "language" : "en"}
 * </pre>
 * After submitting a groupBy query with 24 hours duration
 * <pre>
 * {
 *     "queryType":"groupBy",
 *     "dataSource":"my_dataSource",
 *     "granularity":{"type": "duration", "duration": "86400000"},
 *     "dimensions":[
 *         "language"
 *     ],
 *     "aggregations":[ {
 *         "type":"count",
 *         "name":"count"
 *     } ],
 *     "intervals":[
 *         "2000-01-01T00:00Z/3000-01-01T00:00Z"
 *     ]
 * }
 * </pre>
 * you will get
 * <pre>
 * [
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-08-31T00:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     },
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-01T00:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     },
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-02T00:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     },
 *     {
 *         "version" : "v1",
 *         "timestamp" : "2013-09-03T00:00:00.000Z",
 *         "event" : {
 *             "count" : 1,
 *             "language" : "en"
 *         }
 *     }
 * ]
 * </pre>
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "type", "duration", "origin" })
public class DurationGranularity implements Granularity {

    /**
     * The duration of each time bucket in Druid data response.
     */
    private final Duration duration;

    /**
     * The starting instance of the first time bucket in Druid data response.
     */
    private final DateTime origin;

    /**
     * Constructs a new {@code DurationGranularity} with the specified duration attribute.
     * <p>
     * The origin attribute of this granularity defaults to null and won't be included in serialization.
     *
     * @param duration the duration of each time bucket in Druid data response
     */
    public DurationGranularity(final Duration duration) {
        this(duration, null);
    }

    /**
     * Constructs a new {@code DurationGranularity} with the specified duration and origin attributes.
     *
     * @param duration the duration of each time bucket in Druid data response
     * @param origin the starting instance of the first time bucket in Druid data response
     */
    public DurationGranularity(final Duration duration, final DateTime origin) {
        if (duration == null) {
            throw new NullPointerException("Duration attribute cannot be null");
        }
        this.duration = duration;
        this.origin = origin;
    }

    @JsonProperty(value = "type")
    @Override
    public String getType() {
        return "duration";
    }

    /**
     * Returns the duration attribute of this granularity used for JSON serialization.
     *
     * @return granularity duration for JSON serialization
     */
    @JsonProperty(value = "duration")
    public long getDurationString() {
        return duration.getMillis();
    }

    /**
     * Returns the origin attribute of this granularity used for JSON serialization.
     * <p>
     * This method calls {@link #getOrigin()} as a source of string attribute. If origin is not specified for this
     * granularity, the method returns {@code null}.
     *
     * @return granularity origin for JSON serialization
     */
    @JsonProperty(value = "origin")
    public String getOriginString() {
        final DateTime dateTime = getOrigin();
        return dateTime == null ? null : dateTime.withZone(DateTimeZone.UTC).toString();
    }

    /**
     * Returns the duration as a {@code ReadablePeriod} of this granularity.
     *
     * @return the {@code ReadablePeriod} duration of this granularity
     */
    public Duration getDuration() {
        return duration;
    }


    /**
     * Returns the origin as a {@code DateTime} of this granularity.
     *
     * @return the granularity origin attribute
     */
    public DateTime getOrigin() {
        return origin;
    }

    /**
     * Returns the string representation of this granularity.
     * <p>
     * The format of the string is "DurationGranularity{duration=XXX, origin=YYY}", where XXX is given by
     * {@link #getDuration()} and YYY by {@link #getOrigin()}. Note that two values are separated by a comma followed by
     * a single space. Values are not surrounded by single quotes.
     *
     * @return the string representation of this granularity
     */
    @Override
    public String toString() {
        return String.format("DurationGranularity{duration=%s, origin=%s}", getDuration(), getOrigin());
    }
}
