/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.filter;

/**
 * Model representing an NOT filter expression in a Druid query.
 * <pre>
 * {@code
 *     "filter": {
 *         "type": "not",
 *         "field": <filter>
 *     }
 * }
 * </pre>
 */
@SuppressWarnings("AbstractClassName")
public abstract class NotFilter extends Filter implements ComplexFilter {

    private final Filter field;

    /**
     * Constructs a new {@code NotFilter} with specified filter to "not" over.
     *
     * @param field child filter to "not" over
     */
    public NotFilter(final Filter field) {
        super(DefaultFilterType.NOT);
        this.field = field;
    }

    /**
     * Returns the filter wrapped in this NotFilter.
     *
     * @return the filter wrapped in this NotFilter
     */
    public Filter getField() {
        return field;
    }

    /**
     * TBD.
     *
     * @return tbd
     */
    @Override
    public String toString() {
        return String.format("NOT filter: (%s)", getField().toString());
    }
}
