/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.filter;

import java.util.List;

/**
 * Model representing an AND filter expression in a Druid query.
 * <pre>
 * {@code
 *     "filter": {
 *         "type": "and",
 *         "fields": [<filter1>, <filter2>, ...]
 *     }
 * }
 * </pre>
 */
public class AndFilter extends MultiClauseFilter {

    /**
     * Constructs a new {@code AndFilter} with the specified list of child filters.
     *
     * @param fields filters to AND across
     */
    public AndFilter(final List<Filter> fields) {
        super(DefaultFilterType.AND, fields);
    }
}
