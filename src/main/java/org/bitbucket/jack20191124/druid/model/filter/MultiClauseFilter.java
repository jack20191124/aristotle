/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.filter;

import java.util.Collections;
import java.util.List;

/**
 * Base class for parent filters which take a list of child filters.
 */
@SuppressWarnings("AbstractClassName")
public abstract class MultiClauseFilter extends Filter implements ComplexFilter {

    private final List<Filter> fields;

    /**
     * Constructs a new {@code MultiClauseFilter} with the specified filter type and list of child filters.
     *
     * @param filterType type of the filter
     * @param fields collection of child filters this filter wraps
     */
    protected MultiClauseFilter(final FilterType filterType, final List<Filter> fields) {
        super(filterType);
        this.fields = Collections.unmodifiableList(fields);
    }

    /**
     * Returns an unmodifiable list of the child filters this filter wraps.
     *
     * @return an unmodifiable list of the child filters this filter wraps
     */
    @Override
    public List<Filter> getFields() {
        return fields;
    }
}
