/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.query.searchqueryspec;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Class for specifying the ContainsSearchQuerySpec.
 * <p>
 * If any part of a dimension value contains the value specified in this search query spec, a "match" occurs. The
 * grammar is
 * <pre>
 * {
 *     "type": "contains",
 *     "case_insensitive": true,
 *     "value": "some_value"
 * }
 * </pre>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "case_sensitive", "value" })
public class ContainsSearchQuerySpec extends AbstractSearchQuerySpec {

    /**
     * A flag indicating whether or not the search should be case sensitive.
     */
    private final Boolean caseSensitive;

    /**
     * Value to search for.
     */
    private final String value;

    /**
     * Constructs a new {@code ContainsSearchQuerySpec} with the specified flag on case-sensitive search and value to
     * search for.
     * <p>
     * Both flag and value can be {@code null}.
     *
     * @param caseSensitive a flag indicating whether or not the search should be case sensitive
     * @param value value to search for
     */
    public ContainsSearchQuerySpec(final Boolean caseSensitive, final String value) {
        super(DefaultSearchQuerySpecType.CONTAINS);
        this.caseSensitive = caseSensitive;
        this.value = value;
    }

    /**
     * Returns true if the search is case sensitive.
     *
     * @return the flag indicating whether or not the search should be case sensitive
     */
    @JsonProperty(value = "case_sensitive")
    public Boolean isCaseSensitive() {
        return caseSensitive;
    }

    /**
     * Returns the value to search for in this search query spec.
     *
     * @return the searched value
     */
    public String getValue() {
        return value;
    }

    /**
     * Returns the string representation of this search query spec.
     * <p>
     * <b>The string is NOT the JSON representation used for Druid query.</b> The format of the string is
     * "ContainsSearchQuerySpec{type=XXX, case_sensitive=YYY, value='ZZZ'}", where XXX is given by {@link #getType()},
     * YYY by {@link #isCaseSensitive()}, and ZZZ by {@link #getValue()} which is also surrounded by a pair of single
     * quotes. Note that each value is separated by a comma followed by a single space.
     *
     * @return the string representation of this search query spec
     */
    @Override
    public String toString() {
        return String.format(
                "ContainsSearchQuerySpec{type=%s, case_sensitive=%s, value='%s'}",
                getType(),
                isCaseSensitive(),
                getValue()
        );
    }
}
