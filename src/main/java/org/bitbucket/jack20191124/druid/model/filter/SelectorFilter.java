/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.bitbucket.jack20191124.druid.model.dimension.extractionfunction.ExtractionFunction;

import net.jcip.annotations.Immutable;

import java.util.Objects;

/**
 * Model representing a Selector filter expression in Druid query.
 * <p>
 * The simplest filter is a selector filter. The selector filter will match a specific dimension with a specific
 * value. Selector filters can be used as the base filters for more complex Boolean expressions of filters.
 * <p>
 * The grammar for a {@code SELECTOR} filter is
 * <pre>
 * {@code
 *     "filter": {
 *         "type": "selector",
 *         "dimension": <dimension_string>,
 *         "value": <dimension_value_string>
 *     }
 * }
 * </pre>
 * This is the equivalent of {@code WHERE <dimension_string> = "<dimension_value_string>"}
 * <p>
 * The selector filter supports the use of extraction functions, see the example from class description of
 * {@link AbstractDimensionalFilter}.
 */
@Immutable
@JsonPropertyOrder({ "type", "dimension", "value", "extractionFn" })
@SuppressWarnings("MultipleStringLiterals")
public class SelectorFilter extends AbstractDimensionalFilter {

    /**
     * Value of this filter.
     * <p>
     * For example:
     * <pre>
     * {@code
     *     "filter": {
     *         "type": "selector",
     *         "dimension": <dimension_string>,
     *         "value": <dimension_value_string>
     *     }
     * }
     * </pre>
     */
    private final String value;

    /**
     * Constructs a new {@code SelectorFilter} with the specified filtering dimension and filtering dimension value.
     *
     * @param dimension the dimension to apply the extraction to
     * @param value value of this filter as specified in
     * <pre>
     * {@code
     *     "filter": {
     *         "type": "selector",
     *         "dimension": <dimension_string>,
     *         "value": <dimension_value_string>
     *     }
     * }
     * </pre>
     *
     * @throws NullPointerException if filtering dimension or value is {@code null}.
     */
    public SelectorFilter(final String dimension, final String value) {
        this(dimension, value, null);
    }

    /**
     * Constructs a new {@code SelectorFilter} with the specified filtering dimension, filtering dimension value, and
     * an extraction function to transform the dimension's value before being filtered.
     *
     * @param dimension the dimension to apply the extraction to
     * @param value value of this filter as specified in
     * <pre>
     * {@code
     *     "filter": {
     *         "type": "selector",
     *         "dimension": <dimension_string>,
     *         "value": <dimension_value_string>
     *     }
     * }
     * </pre>
     * @param extractionFunction extraction function to be applied to the dimension
     *
     * @throws NullPointerException if filtering dimension or value is {@code null}.
     */
    public SelectorFilter(
            final String dimension,
            final String value,
            final ExtractionFunction extractionFunction
    ) {
        super(DefaultFilterType.SELECTOR, dimension, extractionFunction);
        this.value = Objects.requireNonNull(value, "value");
    }

    /**
     * Returns value of this filter.
     *
     * @return value of this filter
     */
    @JsonProperty(value = "value")
    public String getValue() {
        return value;
    }
}
