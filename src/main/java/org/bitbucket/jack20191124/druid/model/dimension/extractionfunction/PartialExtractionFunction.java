/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.dimension.extractionfunction;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.regex.Pattern;

/**
 * Partial extraction function returns the dimension value unchanged if the regex matches, otherwise returns null.
 * <pre>
 * {@code
 *     {
 *         "type": "partial",
 *         "expr": <regular_expression>
 *     }
 * }
 * </pre>
 */
public class PartialExtractionFunction extends AbstractExtractionFunction {

    /**
     * Regex pattern of the extraction function.
     */
    private final Pattern pattern;

    /**
     * Constructs a new {@code PartialExtractionFunction} with the specified regex pattern.
     * <p>
     * The provided regex pattern is the value for {@code expr} as in
     * <pre>
     * {@code
     *     {
     *         "type": "partial",
     *         "expr": <regular_expression>
     *     }
     * }
     * </pre>
     *
     * @param pattern regex pattern of the extraction function
     */
    public PartialExtractionFunction(final Pattern pattern) {
        super(DefaultExtractionFunctionType.PARTIAL);
        this.pattern = pattern;
    }

    /**
     * Returns the regex pattern of this extraction function, i.e. the value of {@code expr}.
     *
     * @return the regex pattern of this extraction function
     */
    @JsonProperty(value = "expr")
    public Pattern getPattern() {
        return pattern;
    }

    /**
     * Returns the string representation of this extraction function.
     * <p>
     * The format of the string is "PartialExtractionFunction{pattern=XXX}", where XXX is the string representation of
     * the regex pattern of this extraction function.
     *
     * @return the string representation of this extraction function
     */
    @Override
    public String toString() {
        return String.format("PartialExtractionFunction{pattern=%s}", getPattern());
    }
}
