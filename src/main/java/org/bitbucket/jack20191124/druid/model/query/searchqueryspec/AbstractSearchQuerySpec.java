/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.query.searchqueryspec;

/**
 * Base class for specifying Druid search query spec.
 * <p>
 * Search query specs define how a "match" is defined between a search value and a dimension value. The available search
 * query spec are listed in {@link SearchQuerySpecType}.
 */
public abstract class AbstractSearchQuerySpec {

    /**
     * Type of this search query spec.
     */
    protected final SearchQuerySpecType type;

    /**
     * Constructs a new {@code AbstractSearchQuerySpec} with the specified search query spec type.
     *
     * @param type type for this search query spec
     */
    protected AbstractSearchQuerySpec(final SearchQuerySpecType type) {
        this.type = type;
    }

    /**
     * Returns the type for this search query spec.
     * @return the type for this search query spec
     */
    public SearchQuerySpecType getType() {
        return type;
    }
}
