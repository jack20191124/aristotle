/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.filter;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.bitbucket.jack20191124.druid.model.dimension.extractionfunction.ExtractionFunction;

import java.util.Objects;

/**
 * Filter for matching a dimension.
 * <p>
 * All filters except for the "spatial" filter support extraction functions. An extraction function is defined by
 * setting the {@code extractionFn} filed on a filter. See
 * {@link org.bitbucket.jack20191124.druid.model.dimension.extractionfunction} for more details about extraction
 * functions.
 * <p>
 * If specified, the extraction function will be used to transform input values before the filter is applied. The
 * example below shows a selector filter combined with an extraction function. This filter will transform input values
 * according to the values defined in the lookup map; transformed values will then be matched with the string "bar_1".
 * The following matches dimension values in {@code product_1, product_3, product_5} for the column {@code product}.
 * <pre>
 * {@code
 * {
 *     "filter": {
 *         "type": "selector",
 *         "dimension": "product",
 *         "value": "bar_1",
 *         "extractionFn": {
 *             "type": "lookup",
 *             "map": {
 *                 "product_1": "bar_1",
 *                 "product_5": "bar_1",
 *                 "product_3": "bar_1"
 *             }
 *         }
 *     }
 * }
 * }
 * </pre>
 */
@JsonInclude(NON_NULL)
public abstract class AbstractDimensionalFilter extends Filter {

    /**
     * Filtering dimension.
     */
    private final String dimension;

    /**
     * Extraction function to be applied on dimension.
     */
    private final ExtractionFunction extractionFunction;

    /**
     * Constructs a new {@code AbstractDimensionalFilter} with the specified filter type, filtering dimension.
     *<p>
     * No extraction function will be specified for the filter instance created by this constructor.
     *
     * @param filterType type of the filter
     * @param dimension filtering dimension
     *
     * @throws NullPointerException if the filtering dimension is {@code null}
     */
    public AbstractDimensionalFilter(
            final FilterType filterType,
            final String dimension
    ) {
        this(filterType, dimension, null);
    }

    /**
     * Constructs a new {@code AbstractDimensionalFilter} with the specified filter type, filtering dimension, and
     * extraction function.
     *
     * @param filterType type of the filter
     * @param dimension filtering dimension
     * @param extractionFunction extraction function to be applied on dimension
     *
     * @throws NullPointerException if the filtering dimension is {@code null}
     */
    public AbstractDimensionalFilter(
            final FilterType filterType,
            final String dimension,
            final ExtractionFunction extractionFunction
    ) {
        super(filterType);
        this.dimension = Objects.requireNonNull(dimension, "dimension");
        this.extractionFunction = extractionFunction;
    }

    /**
     * Returns the filtering dimension of this filter.
     *
     * @return the filtering dimension of this filter
     */
    @JsonProperty(value = "dimension")
    public String getDimension() {
        return dimension;
    }

    /**
     * Returns the extraction function to be applied on dimension.
     *
     * @return the extraction function to be applied on dimension
     */
    @JsonProperty(value = "extractionFn")
    public ExtractionFunction getExtractionFunction() {
        return extractionFunction;
    }
}
