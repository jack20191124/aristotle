/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.dimension.extractionfunction;

/**
 * Extraction functions define the transformation applied to each dimension value.
 * <p>
 * Transformation can be applied to both regular (string) dimension, as well as the special {@code __time} dimension,
 * which represents the current time bucket according to the query aggregation granularity. For functions taking string
 * values (such as regular expressions), {@code __time} dimension values should be formatted in ISO-8601 format before
 * getting passed to the extraction function.
 */
public abstract class AbstractExtractionFunction implements ExtractionFunction {

    /**
     * The type of this extraction function.
     */
    private final ExtractionFunctionType type;

    /**
     * Constructs a new {@code ExtractionFunction} with the specified extraction function type.
     *
     * @param type type of this extraction function
     */
    protected AbstractExtractionFunction(final ExtractionFunctionType type) {
        this.type = type;
    }

    /**
     * Returns the type of this extraction function.
     *
     * @return the type of this extraction function
     */
    public ExtractionFunctionType getType() {
        return type;
    }

    /**
     * Enumeration of possible extraction function types.
     */
    public enum DefaultExtractionFunctionType implements ExtractionFunctionType {

        /**
         * Regular expression extraction function.
         * <p>
         * See {@link RegexExtractionFunction}.
         */
        REGEX,

        /**
         * Partial extraction function.
         * <p>
         * See {@link PartialExtractionFunction}.
         */
        PARTIAL,
    }
}
