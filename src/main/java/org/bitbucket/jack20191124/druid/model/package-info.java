/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * <h1>Serialization Strategy</h1>
 * The package uses Jackson annotations to map Java objects to their JSON representations. Specifically, the following
 * annotations are used
 * <ul>
 *     <li> <b>{@code @JsonValue}</b>. The jackson annotation {@code @JsonValue} tells Jackson that Jackson should not
 *          attempt to serialize the object itself, but rather call a method on the object which serializes the object
 *          to a JSON string. Note that Jackson will escape any quotation marks inside the String returned by the custom
 *          serialization.
 *          <p>
 *          The {@code @JsonValue} annotation is added to the method that Jackson is to call to serialize the object
 *          into a JSON string. Here is an example showing how to use the {@code @JsonValue} annotation:
 *          <pre>
 *          {@code
 *              public class Person {
 *                  private final personId;
 *                  private String name;
 *
 *                  ...
 *
 *                  {@literal @}JsonValue
 *                  public String toJson() {
 *                      return String.format("%s, %s", personId, name);
 *                  }
 *              }
 *          }
 *          </pre>
 * </ul>
 */
package org.bitbucket.jack20191124.druid.model;
