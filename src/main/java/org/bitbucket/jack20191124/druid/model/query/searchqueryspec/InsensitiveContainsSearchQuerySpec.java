/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.query.searchqueryspec;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Class for specifying the InsensitiveContainsSearchQuerySpec.
 * <p>
 * If any part of a dimension value contains the value specified in this search query spec, regardless of case, a
 * "match" occurs. The grammar is
 * <pre>
 * {
 *     "type": "insensitive_contains",
 *     "value": "some_value"
 * }
 * </pre>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "value" })
public class InsensitiveContainsSearchQuerySpec extends AbstractSearchQuerySpec {

    /**
     * Value to search for case-insensitivity.
     */
    private final String value;

    /**
     * Constructs a new {@code InsensitiveContainsSearchQuerySpec} with the specified search value.
     * <p>
     * The value can be {@code null}.
     *
     * @param value value to search for case-insensitivity
     */
    public InsensitiveContainsSearchQuerySpec(final String value) {
        super(DefaultSearchQuerySpecType.INSENSITIVE_CONTAINS);
        this.value = value;
    }

    /**
     * Returns the value to search for case-insensitivity in this search query spec.
     *
     * @return the searched value
     */
    public String getValue() {
        return value;
    }

    /**
     * Returns the string representation of this search query spec.
     * <p>
     * <b>The string is NOT the JSON representation used for Druid query.</b> The format of the string is
     * "InsensitiveContainsSearchQuerySpec{type=XXX, value=YYY}", where XXX is given by {@link #getType()} and YYY by
     * {@link #getValue()} which is also surrounded by single quotes. Note that each value is separated by a comma
     * followed by a single space.
     *
     * @return the string representation of this search query spec
     */
    @Override
    public String toString() {
        return String.format(
                "InsensitiveContainsSearchQuerySpec{type=%s, value='%s'}",
                getValue(),
                getType()
        );
    }
}
