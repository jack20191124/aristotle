/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.filter;

import java.util.List;

/**
 * A Druid filter that is defined by applying an operation(such as AND) on multiple filters. For example,
 * {@link NotFilter} and {@link AndFilter} are complex, whereas {@link SelectorFilter} is not.
 */
public interface ComplexFilter {

    /**
     * Returns the filters that are operated on by this complex filter.
     *
     * @return the filters that are operated on by this complex filter
     */
    List<Filter> getFields();
}
