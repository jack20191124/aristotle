/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.dimension.extractionfunction;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.regex.Pattern;

/**
 * Regular Expression ExtractionFunction returns the first matching group for the given regular expression.
 * <p>
 * If there is no match, it returns the dimension value as is. The actual serialized extraction function will be
 * <pre>
 * {@code
 *     {
 *         "type": "regex",
 *         "expr": <regular_expression>
 *     }
 * }
 * </pre>
 * <b>Note that the {@code index}, {@code replaceMissingValue}, and {@code replaceMissingValueWith} are using default
 * values and are not included in the serialization</b>. If you would like to have customized values for them, you could
 * read the documentation below and make your own regex extraction function.
 * <pre>
 * {@code
 *     {
 *         "type": "regex",
 *         "expr": <regular_expression>,
 *         "index": <group to extract, default to 1>,
 *         "replaceMissingValue": true,
 *         "replaceMissingValueWith": "foobar"
 *     }
 * }
 * </pre>
 * For example, using {@code "expr": "(\\w\\w\\w).*"} will transform {@code Monday}, {@code Tuesday},
 * {@code Wednesday} into {@code Mon}, {@code Tue}, {@code Wed}.
 * <p>
 * If "index" is set, it will control which group from the match to extract. Index 0 extracts the string
 * matching the entire pattern.
 * <p>
 * If the {@code replaceMissingValue} property is true, the extraction function will transform dimension values
 * that do not match the regex pattern to a user-specified String. Default value is {@code false}.
 * <p>
 * The {@code replaceMissingValueWith} property sets the String that unmatched dimension values will be replaced
 * with, if {@code replaceMissingValue} is true. If {@code replaceMissingValueWith} is not specified, unmatched
 * dimension values will be replaced with nulls.
 */
public class RegexExtractionFunction extends AbstractExtractionFunction {

    /**
     * Regex pattern of the extraction function.
     */
    private final Pattern pattern;

    /**
     * Constructs a new {@code RegexExtractionFunction} with the specified regex pattern.
     * <p>
     * The provided regex pattern is the value for {@code expr} as in
     * <pre>
     * {@code
     *     {
     *         "type": "regex",
     *         "expr": <regular_expression>
     *     }
     * }
     * </pre>
     *
     * @param pattern regex pattern of the extraction function
     */
    public RegexExtractionFunction(final Pattern pattern) {
        super(DefaultExtractionFunctionType.REGEX);
        this.pattern = pattern;
    }

    /**
     * Returns the regex pattern of this extraction function, i.e. the value of {@code expr}.
     *
     * @return the regex pattern of this extraction function
     */
    @JsonProperty(value = "expr")
    public Pattern getPattern() {
        return pattern;
    }

    /**
     * Returns the string representation of this extraction function.
     * <p>
     * The format of the string is "RegexExtractionFunction{pattern=XXX}", where XXX is the string representation of
     * the regex pattern of this extraction function.
     *
     * @return the string representation of this extraction function
     */
    @Override
    public String toString() {
        return String.format("RegexExtractionFunction{pattern=%s}", getPattern().pattern());
    }
}
