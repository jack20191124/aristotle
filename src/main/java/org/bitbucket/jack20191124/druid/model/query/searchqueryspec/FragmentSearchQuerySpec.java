/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.query.searchqueryspec;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Class for specifying the FragmentSearchQuerySpec.
 * <p>
 * If any part of a dimension value contains all of the values specified in this search query spec, regardless of case
 * by default, a "match" occurs. The grammar is
 * <pre>
 * {
 *     "type": "fragment",
 *     "case_insensitive": false,
 *     "values": ["fragment1", "fragment2"]
 * }
 * </pre>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "case_sensitive", "values" })
public class FragmentSearchQuerySpec extends AbstractSearchQuerySpec {

    /**
     * A flag indicating whether or not the search should be case sensitive.
     */
    private final Boolean caseSensitive;

    /**
     * Fragments to search for.
     */
    private final List<String> values;

    /**
     * Constructs a new {@code FragmentSearchQuerySpec} with the specified flag on case-sensitive search and fragments
     * to search for.
     * <p>
     * Both flag and fragments can be {@code null}.
     *
     * @param caseSensitive a flag indicating whether or not the search should be case sensitive
     * @param values fragments to search for
     */
    public FragmentSearchQuerySpec(final Boolean caseSensitive, final Collection<String> values) {
        super(DefaultSearchQuerySpecType.FRAGMENT);
        this.caseSensitive = caseSensitive;
        this.values = values == null ? null : Collections.unmodifiableList(new ArrayList<>(values));
    }

    /**
     * Returns true if the search is case sensitive.
     *
     * @return the flag indicating whether or not the search should be case sensitive
     */
    @JsonProperty(value = "case_sensitive")
    public Boolean isCaseSensitive() {
        return caseSensitive;
    }

    /**
     * Returns the fragments to search for in the search query spec.
     *
     * @return the searched fragments
     */
    public List<String> getValues() {
        return values;
    }

    /**
     * Returns the string representation of this search query spec.
     * <p>
     * <b>The string is NOT the JSON representation used for Druid query.</b> The format of the string is
     * "FragmentSearchQuerySpec{type=XXX, case_sensitive=YYY, values=ZZZ}", where XXX is given by {@link #getType()},
     * YYY by {@link #isCaseSensitive()}, and ZZZ by {@link #getValues()}. Note that each value is
     * separated by a comma followed by a single space. Values are not quoted.
     *
     * @return the string representation of this search query spec
     */
    @Override
    public String toString() {
        return String.format(
                "FragmentSearchQuerySpec{type=%s, case_sensitive=%s, values=%s}",
                getType(),
                isCaseSensitive(),
                getValues()
        );
    }
}
