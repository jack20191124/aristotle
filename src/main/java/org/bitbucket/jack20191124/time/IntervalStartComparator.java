/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.time;

import org.joda.time.Interval;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Compares intervals based on their starting instant.
 * <p>
 * Note: this comparator imposes orderings that are inconsistent with equals. Only the starting instant is considered.
 */
public class IntervalStartComparator implements Comparator<Interval>, Serializable {
    /**
     * A static instance of {@link IntervalStartComparator}.
     */
    public static final IntervalStartComparator INSTANCE = new IntervalStartComparator();

    @Override
    public int compare(final Interval first, final Interval second) {
        return first.getStart().compareTo(second.getStart());
    }
}
