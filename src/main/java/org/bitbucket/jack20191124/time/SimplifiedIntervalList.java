/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.time;

import org.apache.commons.collections4.IteratorUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.ReadablePeriod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.NotNull;

/**
 * A simplified interval list is a list of intervals, ordered by time, expressed in as few intervals as possible (i.e.
 * adjacent and overlapping intervals are combined into a single interval).
 */
public class SimplifiedIntervalList extends LinkedList<Interval> {
    /**
     * A {@link java.util.function.Function} to iterate an {@link java.util.Iterator} if it has a next element,
     * otherwise return null.
     */
    protected static final Function<Iterator<Interval>, Interval> GET_NEXT_IF_AVAILABLE =
            (it) -> it.hasNext() ? it.next() : null;

    /**
     * An error message for {@link #add(Interval)} operation.
     */
    private static final String ADD_OPERATION_NOT_SUPPORTED = "Do not use add in Simplified Interval List";

    /**
     * Constructor.
     */
    public SimplifiedIntervalList() {
        super();
    }

    /**
     * Constructor.
     * <p>
     * Simplifies then builds a list.
     *
     * @param intervals  A collection of {@link org.joda.time.Interval intervals}
     */
    public SimplifiedIntervalList(final Collection<Interval> intervals) {
        super(simplifyIntervals(intervals));
    }

    /**
     * Constructor.
     * <p>
     * If the intervals are already simplified, simply copy the list.
     *
     * @param simplifiedIntervalList  An instance of {@link org.bitbucket.jack20191124.time.SimplifiedIntervalList}
     */
    public SimplifiedIntervalList(final SimplifiedIntervalList simplifiedIntervalList) {
        super(simplifiedIntervalList);
    }

    /**
     * Converts this {@link SimplifiedIntervalList} to a regular {@link java.util.List} to
     * address deserialization issue.
     *
     * @return list of simplified intervals
     */
    public List<Interval> asList() {
        return Collections.unmodifiableList(this);
    }

    /**
     * Takes one or more list of intervals, and combines them into a single, sorted list with the minimum number of
     * intervals needed to capture exactly the same instants as the original intervals.
     * <p>
     * If any sub-intervals of the input collection abut or overlap, they will be replaced with a single, combined
     * interval.
     * <p>
     * For example:
     * <ul>
     *     <li>['2014/2017', '2015/2020'] will combine into ['2014/2020']</li>
     *     <li>['2015/2016', '2016/2017'] will combine into ['2015/2017']</li>
     *     <li>['2015/2016', '2013/2014'] will sort into ['2013/2014', '2015/2016']</li>
     *     <li>['2015/2015', '2015/2016', '2012/2013'] will sort and combine to ['2012/2013', '2015/2016']</li>
     * </ul>
     *
     * @param intervals  The collection(s) of intervals being collated
     *
     * @return a single list of sorted intervals simplified to the smallest number of intervals able to describe the
     * duration
     */
    @SafeVarargs
    public static SimplifiedIntervalList simplifyIntervals(final Collection<Interval>... intervals) {
        Stream<Interval> allIntervals = Stream.empty();
        for (final Collection<Interval> intervalCollection : intervals) {
            allIntervals = Stream.concat(allIntervals, intervalCollection.stream());
        }

        return allIntervals.sorted(IntervalStartComparator.INSTANCE::compare).collect(getCollector());
    }

    /**
     * Builds a {@link java.util.stream.Collector} which appends overlapping intervals, merging and simplifying as it
     * goes.
     *
     * @return a collector for merging simplified intervals
     */
    public static Collector<Interval, SimplifiedIntervalList, SimplifiedIntervalList> getCollector() {
        return Collector.of(
                SimplifiedIntervalList::new,
                SimplifiedIntervalList::appendWithMerge,
                /*
                when partitions are joined and simplifyIntervals() calls getCollector() from inside, this combiner will
                not be called, but appendWithMerge(), because join is not concurrent. So there is no circular dependency
                between simplifyIntervals() and getCollector()
                 */
                SimplifiedIntervalList::simplifyIntervals
        );
    }

    /**
     * Given a sorted linked list of intervals, add an interval to the end, merging the incoming interval to any tail
     * intervals which overlap or abut with it.
     * <p>
     * In the case where added intervals are at the end of the list, this is efficient. In the case where they are not,
     * this degrades to an insertion sort.
     *
     * @param interval  The interval to be merged and added to this list
     */
    private void appendWithMerge(final Interval interval) {
        // Do not store empty intervals
        if (interval.toDurationMillis() == 0) {
            return;
        }

        if (isEmpty()) {
            addLast(interval);
            return;
        }

        final Interval previous = peekLast();

        if (interval.getStart().isBefore(previous.getStart())) {
            mergeInner(interval);
            return;
        }

        // the entire interval is after the last interval
        if (previous.gap(interval) != null) {
            addLast(interval);
            return;
        }

        // at this point, it is guaranteed that interval.start is after previous.previous.end
        removeLast();
        final Interval newEnd = new Interval(
                Math.min(previous.getStartMillis(), interval.getStartMillis()),
                Math.max(previous.getEndMillis(), interval.getEndMillis())
        );
        addLast(newEnd);
    }

    /**
     * Backs elements of the list until the insertion is at the correct endpoint of the list and then merge and append
     * the original contents of the list back in.
     *
     * @param interval  The interval to be merged and added
     */
    private void mergeInner(final Interval interval) {
        Interval previous = peekLast();
        final LinkedList<Interval> buffer = new LinkedList<>();

        while (previous != null && interval.getStart().isBefore(previous.getStart())) {
            buffer.addFirst(previous);
            removeLast();
            previous = peekLast();
        }

        /*
        at this point, mergeInner is guaranteed not being called again from inside appendWithMerge()
         */
        appendWithMerge(interval);
        buffer.stream().forEach(this::appendWithMerge);
    }

    /**
     * A {@link java.util.function.Predicate} to scan a {@link SimplifiedIntervalList} using
     * an {@link java.util.Iterator iterator} and a test {@link java.util.function.Predicate predicate}.
     * <p>
     * An {@link java.util.Iterator iterator} over the list is scanned until an interval not fully before the interval
     * under test is found. If no such interval exists, a default value is returned. This
     * {@link java.util.function.Predicate} can be reused as long as each subsequent call to the test has an equal or
     * later start data.
     */
    public static class SkippingIntervalPredicate implements Predicate<Interval> {
        /**
         * The value for the test if no comparison interval can be found in the list.
         */
        private final boolean defaultValue;
        /**
         * The {@link java.util.Iterator iterator} of the intervals to test the predicate against.
         */
        private final Iterator<Interval> supply;
        /**
         * The current {@link org.joda.time.Interval} from the {@link #supply}.
         */
        private Interval activeInterval;
        /**
         * The {@link java.util.function.Predicate} to use when testing an interval against the supply.
         */
        private final BiPredicate<Interval, Interval> testPredicate;

        /**
         * Constructor.
         * <p>
         * Builds a {@link java.util.function.Predicate} that applies an arbitrary predicate to not-before intervals
         * from the iterator.
         *
         * @param supplyList  The {@link SimplifiedIntervalList} of intervals to test the predicate against
         * @param testPredicate  The {@link java.util.function.Predicate} to use when testing an interval against the
         * supply
         * @param defaultValue  The value for the test if no comparison interval can be found in the list
         */
        public SkippingIntervalPredicate(
                final SimplifiedIntervalList supplyList,
                final BiPredicate<Interval, Interval> testPredicate,
                final boolean defaultValue
        ) {
            this.supply = supplyList.iterator();
            this.testPredicate = testPredicate;
            this.defaultValue = defaultValue;

            activeInterval = null;
            if (supply.hasNext()) {
                activeInterval = supply.next();
            }
        }

        /**
         * Skips through the supply intervals until an active interval matches (that is, one which is at or after the
         * test interval) is located and then test it against the testPredicate.
         * <p>
         * If no comparision interval is found, return a default value.
         *
         * @param testInterval  The {@link org.joda.time.Interval} to test against an active interval
         *
         * @return  the result of the test {@link java.util.function.Predicate predicate} with an active interval
         * otherwise default value.
         */
        @Override
        public boolean test(final Interval testInterval) {
            skipAhead(testInterval.getStart());
            return activeInterval == null ? defaultValue : testPredicate.test(testInterval, activeInterval);
        }

        /**
         * Skips ahead to the indicated {@link org.joda.time.DateTime}.
         *
         * @param skipAheadTo  Instant to skip to
         */
        private void skipAhead(final @NotNull DateTime skipAheadTo) {
            while (activeInterval != null && activeInterval.isBefore(skipAheadTo)) {
                activeInterval = supply.hasNext() ? supply.next() : null;
            }
        }
    }

    /**
     * A {@link java.util.function.Predicate} for testing whether a test interval is a complete sub-interval of part of
     * the supply of intervals.
     */
    public static class IsSubinterval extends SkippingIntervalPredicate {
        /**
         * Filter in intervals from the stream that are fully contained by the supply.
         */
        public static final BiPredicate<Interval, Interval> IS_SUBINTERVAL =
                (test, supplyInterval) -> supplyInterval.contains(test);

        /**
         * Constructor.
         * <p>
         * Constructs a sub-interval predicate that closes over a supply of intervals.
         *
         * @param supplyList  The intervals to test an interval is contained by
         */
        public IsSubinterval(final SimplifiedIntervalList supplyList) {
            super(supplyList, IS_SUBINTERVAL, false);
        }
    }

    /**
     * Only internal calls to linked list mutators should be used to change the list.
     *
     * @param interval  An element to be added
     *
     * @return nothing, a runtime exception is always thrown
     */
    @Override
    public boolean add(final Interval interval) {
        throw new IllegalAccessError(ADD_OPERATION_NOT_SUPPORTED);
    }

    /**
     * Returns union of this simplified interval list and the intervals of another.
     *
     * @param that  A simplified list of intervals
     *
     * @return a new simplified interval list containing all sub-intervals of both this and that
     */
    public SimplifiedIntervalList union(final SimplifiedIntervalList that) {
        return simplifyIntervals(this, that);
    }

    /**
     * Returns intersection of all sub-intervals in two simplified interval lists.
     *
     * @param that  A simplified list of intervals
     *
     * @return a new simplified interval list whose intervals are all sub-intervals of this and that.
     */
    public SimplifiedIntervalList intersect(final SimplifiedIntervalList that) {
        final Iterator<Interval> theseIntervals = this.iterator();
        final Iterator<Interval> thoseIntervals = that.iterator();
        Interval thisCurrent = GET_NEXT_IF_AVAILABLE.apply(theseIntervals);
        Interval thatCurrent = GET_NEXT_IF_AVAILABLE.apply(thoseIntervals);

        final List<Interval> collected = new ArrayList<>();

        while (thisCurrent != null && thatCurrent != null) {
            if (thisCurrent.overlaps(thatCurrent)) {
                collected.add(thisCurrent.overlap(thatCurrent));
            }

            if (thisCurrent.isBefore(thatCurrent.getEnd())) {
                thisCurrent = GET_NEXT_IF_AVAILABLE.apply(theseIntervals);
            } else {
                thatCurrent = GET_NEXT_IF_AVAILABLE.apply(thoseIntervals);
            }
        }

        return new SimplifiedIntervalList(collected);
    }

    /**
     * Returns the subtracted list of all intervals in this that are not in that.
     *
     * @param that  A simplified interval list
     *
     * @return a new simplified interval list whose intervals are all sub-intervals of this and not that
     */
    public SimplifiedIntervalList subtract(final SimplifiedIntervalList that) {
        final Iterator<Interval> theseIntervals = this.iterator();
        Interval thisCurrent = GET_NEXT_IF_AVAILABLE.apply(theseIntervals);

        if (thisCurrent == null) {
            return new SimplifiedIntervalList();
        }

        final Iterator<Interval> thoseIntervals = that.iterator();
        Interval thatCurrent = GET_NEXT_IF_AVAILABLE.apply(thoseIntervals);

        final List<Interval> collected = new ArrayList<>();

        while (thisCurrent != null && thatCurrent != null) {
            if (thisCurrent.isBefore(thatCurrent)) {
                // Non-overlapping intervals are simply collected
                collected.add(thisCurrent);
            } else if (thisCurrent.overlaps(thatCurrent)) {
                // Take any part of the source interval that lies before an overlap
                if (thisCurrent.getStart().isBefore(thatCurrent.getStart())) {
                    collected.add(new Interval(thisCurrent.getStart(), thatCurrent.getStart()));
                }
                // Truncate out any overlap from the source interval and continue
                if (!thisCurrent.getEnd().isBefore(thatCurrent.getEnd())) {
                    thisCurrent = new Interval(thatCurrent.getEnd(), thisCurrent.getEnd());
                }
            }

            // Advance to the next iterator
            if (thisCurrent.isBefore(thatCurrent.getEnd())) {
                thisCurrent = GET_NEXT_IF_AVAILABLE.apply(theseIntervals);
            } else {
                thatCurrent = GET_NEXT_IF_AVAILABLE.apply(thoseIntervals);
            }
        }

        if (thatCurrent == null) {
            collected.add(thisCurrent);
            while (theseIntervals.hasNext()) {
                collected.add(theseIntervals.next());
            }
        }

        return new SimplifiedIntervalList(collected);
    }

    /**
     * Creat an {@link java.util.Iterator} using {@link org.bitbucket.jack20191124.time.IntervalPeriodIterator} to all
     * {@link org.joda.time.Interval intervals} of this list broken up into pieces of size
     * {@link org.joda.time.ReadablePeriod period}.
     *
     * @param readablePeriod  The {@link org.joda.time.ReadablePeriod period} to chunk sub-intervals into
     *
     * @return an {@link java.util.Iterator} which returns {@link org.joda.time.ReadablePeriod period} sized
     * sub-intervals of this interval list
     */
    public Iterator<Interval> periodIterator(final ReadablePeriod readablePeriod) {
        return IteratorUtils.chainedIterator(
                this.stream().map(interval -> new IntervalPeriodIterator(readablePeriod, interval))
                .collect(Collectors.toList())
        );
    }
}
