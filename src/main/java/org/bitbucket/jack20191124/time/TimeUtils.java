/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.time;

import java.time.Clock;
import java.time.Instant;

/**
 * Assorted methods for dealing with times.
 */
public class TimeUtils {

    /**
     * Constructor.
     * <p>
     * Suppress default constructor for noninstantiability.
     */
    private TimeUtils() {
        throw new AssertionError();
    }

    /**
     * Obtains the current time, as number of seconds from the Java epoch of 1970-01-01T00:00:00Z, from the system
     * clock.
     * <p>
     * This will query the {@link Clock#systemUTC() system UTC clock} to obtain the current time.
     * <p>
     * Using this method will prevent the ability to use an alternate time-source for testing because the clock is
     * effectively hard-coded.
     * <p>
     * The epoch second count is a simple incrementing count of seconds where second 0 is 1970-01-01T00:00:00Z. The
     * nanosecond part of the day is returned by {@code getNanosOfSecond}.
     *
     * @return the seconds from the epoch of 1970-01-01T00:00:00Z
     */
    public static long getCurrentUnixTimeInSeconds() {
        return Instant.now().getEpochSecond();
    }
}
