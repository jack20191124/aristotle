/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.time;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.joda.time.Interval;

import java.util.Collection;
import java.util.Iterator;

/**
 * Granularity determines how data gets bucketed across the time dimension, or how it gets aggregated by hour, day,
 * minute, etc.
 * <p>
 * This is a very useful notion for describing time keyed data.
 */
public interface Granularity {

    /**
     * Returns the name of the {@link Granularity}.
     *
     * @return the {@link Granularity} name
     */
    @JsonIgnore
    String getName();

    /**
     * Returns an {@link java.util.Iterator} that will iterate over a set of {@link org.joda.time.Interval intervals},
     * collecting them into a single stream, and slicing or not slicing them according to the granularity given.
     *
     * @param intervals  The intervals being iterated across
     *
     * @return a grain delimited {@link java.util.Iterator} returning all the {@link org.joda.time.Interval intervals}
     * in the interval set
     */
    default Iterator<Interval> intervalsIterator(final Collection<Interval> intervals) {
        return intervalIterator(
                intervals instanceof SimplifiedIntervalList
                        ? (SimplifiedIntervalList) intervals
                        : new SimplifiedIntervalList(intervals)
        );
    }

    /**
     * Returns an {@link java.util.Iterator} from a
     * {@link org.bitbucket.jack20191124.time.SimplifiedIntervalList pre-simplified list of intervals}.
     *
     * @param simplifiedIntervalList  The intervals as a
     * {@link org.bitbucket.jack20191124.time.SimplifiedIntervalList simplified interval list}.
     *
     * @return a grain delimited {@link java.util.Iterator} returning all the {@link org.joda.time.Interval intervals}
     * in the interval set
     */
    Iterator<Interval> intervalIterator(SimplifiedIntervalList simplifiedIntervalList);

    /**
     * Wraps the granularity iterator in an iterable predicate for use in stream.
     *
     * @param intervals  The {@link org.joda.time.Interval intervals} being iterated across
     *
     * @return an iterable useful for streaming over {@link org.joda.time.Interval intervals} by grain
     */
    default Iterable<Interval> intervalIterable(final Collection<Interval> intervals) {
        return () -> intervalsIterator(intervals);
    }

    /**
     * Determine if this {@link Granularity} can be fulfilled by an aggregate of another
     * {@link Granularity}.
     *
     * @param that  The {@link Granularity} to be compared against
     *
     * @return true if this {@link Granularity} can be expressed in terms of elements of
     * that {@link Granularity}
     */
    boolean satisfiedBy(Granularity that);

    /**
     * Determines the reciprocal relationship to {@link #satisfiedBy(Granularity)}, that this
     * {@link Granularity} fulfills an aggregate of another
     * {@link Granularity}.
     *
     * @param that  The {@link Granularity} to be compared against
     *
     * @return true if that {@link Granularity} can be expressed in terms of elements of
     * this {@link Granularity}
     */
    default boolean satisfied(final Granularity that) {
        return that.satisfiedBy(this);
    }

    /**
     * Determines if all {@link org.joda.time.Interval intervals} align with a
     * {@link Granularity}.
     *
     * @param intervals  The {@link java.util.Collection collection} of {@link org.joda.time.Interval intervals} that
     * should be checked
     *
     * @return true if all of the {@link org.joda.time.Interval intervals} start and end on instants which this
     * {@link Granularity} bounds or false, otherwise
     */
    boolean accepts(Collection<Interval> intervals);

    /**
     * Returns description of correct alignment of a {@link Granularity}, e.g. week
     *
     * @return the correct alignment description
     */
    String getAlignmentDescription();
}
