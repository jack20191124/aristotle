/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.time;

import static org.bitbucket.jack20191124.jaxrs.config.ErrorMessageFormat.UNKNOWN_GRANULARITY;

/**
 * Thrown when there is a problem parsing a string into a {@link Granularity}.
 */
public class GranularityParseException extends Exception {
    /**
     * Constructor.
     *
     * @param value  String that was not able to be parsed into a {@link Granularity}
     */
    public GranularityParseException(final String value) {
        super(UNKNOWN_GRANULARITY.format(value));
    }
}
