/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.time;

import org.apache.commons.lang3.ObjectUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.ReadablePeriod;

import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.validation.constraints.NotNull;

/**
 * An {@link java.util.Iterator} that splits an {@link org.joda.time.Interval} into slices of length equal to a
 * {@link org.joda.time.ReadablePeriod period} and returns them.
 * <p>
 * The slices returned are aligned to the interval start. Any partial slice at the end will return the remaining time.
 * <p>
 * For example, period of "P1Y" over interval "2018/2020-02-01" will result in an
 * {@link org.bitbucket.jack20191124.time.IntervalPeriodIterator} of
 * <pre>
 *     {@code
 *     iterator.next() -> "2018/2019"
 *     iterator.next() -> "2019/2020"
 *     iterator.next() -> "2020/2020-02-01"
 *     }
 * </pre>
 */
public class IntervalPeriodIterator implements Iterator<Interval> {
    /**
     * The {@link org.joda.time.ReadablePeriod period} to divide the interval by.
     */
    private final ReadablePeriod readablePeriod;
    /**
     * The starting instance of time for the next slice.
     */
    private final DateTime intervalStart;
    /**
     * The ending instance of time of {@link org.joda.time.Interval} which is to be divided.
     */
    private final DateTime intervalEnd;

    /**
     * The current number of periods from the start of the interval.
     */
    private int position;
    /**
     * The starting instance of time of the next slice to be returned.
     */
    private DateTime currentPosition;

    /**
     * Constructor.
     *
     * @param readablePeriod  The {@link org.joda.time.ReadablePeriod period} to divide the interval by
     * @param interval  The {@link org.joda.time.Interval} which is to be divided
     */
    public IntervalPeriodIterator(@NotNull final ReadablePeriod readablePeriod, final Interval interval) {
        this.readablePeriod = readablePeriod;
        intervalStart = interval.getStart();
        intervalEnd = interval.getEnd();
        position = 0;
        currentPosition = boundaryAt(0);
    }

    @Override
    public boolean hasNext() {
        return currentPosition.isBefore(intervalEnd);
    }

    @Override
    public Interval next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }

        position += 1;
        final DateTime nextPosition = ObjectUtils.min(intervalEnd, boundaryAt(position));
        final Interval result = new Interval(currentPosition, nextPosition);
        currentPosition = nextPosition;
        return result;
    }

    /**
     * Returns the start of a sub-interval at a period based offset from the interval start.
     *
     * @param n  The number of periods from the start of the {@link org.joda.time.Interval}
     *
     * @return the calculated {@link org.joda.time.DateTime instant}
     */
    private DateTime boundaryAt(final int n) {
        return new DateTime(intervalStart.getChronology().add(readablePeriod, intervalStart.getMillis(), n));
    }
}
