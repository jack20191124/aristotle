/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Utility class to tokenize Strings based on CSV properties.
 */
public class FilterTokenizer {
    /**
     * Error message format at CSV parsing error.
     */
    private static final String PARSING_FAILURE = "Parsing values '%s' failed";
    /**
     * Error message format when CSV input has empty items.
     */
    private static final String PARSING_FAILURE_UNQUOTED_VALUES_FORMAT = "Set of values '%s' cannot be processed. " +
            "Empty strings, commas, and leading or trailing whitespace must be quoted";

    /**
     * The default/actual CSV parser.
     * <p>
     * Sharing ObjectReaders/Writers is recommended as more efficient v.s. sharing ObjectMapper. It is thread-safe. Read
     * https://github.com/FasterXML/jackson-docs/wiki/Presentation:-Jackson-Performance" for more details.
     */
    private static ObjectReader defaultReader = initDefault();
    /**
     * The CSV parser that does not put double quotes around parsed token.
     * <p>
     * Since there is no way to check whether a field was an empty string surrounded by double quites or not after
     * parsing it as a CSV string with the {@link #defaultReader default parser}, we use 2 parsers.
     * <ol>
     *     <li>one with the default schema - {@link #defaultReader}</li>
     *     <li>one that does not translate quotes - {@link #rawReader}</li>
     * </ol>
     * If the one that does not translate quotes includes empty strings (after trimming) then it is unclear whether
     * the use of empty field was intended or not by the user and therefore an exception is thrown.
     */
    private static ObjectReader rawReader = initRaw();

    /**
     * Initialize an {@link com.fasterxml.jackson.databind.ObjectReader}.
     *
     * @return an initialized {@link com.fasterxml.jackson.databind.ObjectReader}
     */
    protected static ObjectReader initDefault() {
        return init(CsvSchema.emptySchema());
    }

    /**
     * Initialize an {@link com.fasterxml.jackson.databind.ObjectReader} that does not quote things.
     *
     * @return an initialized {@link com.fasterxml.jackson.databind.ObjectReader}
     */
    protected static ObjectReader initRaw() {
        return init(CsvSchema.emptySchema().withoutQuoteChar());
    }

    /**
     * Initializes the tokenizer.
     *
     * @param filterCsvSchema  Schame to initialize the tokenizer for
     *
     * @return an initialized {@link com.fasterxml.jackson.databind.ObjectReader}
     */
    protected static ObjectReader init(final CsvSchema filterCsvSchema) {
        return new CsvMapper()
                .disable(CsvParser.Feature.WRAP_AS_ARRAY)
                .readerFor(String[].class)
                .with(filterCsvSchema);
    }

    /**
     * Splits a string into tokens based on CSV rules.
     * <p>
     * The CSV parsing defined here uses comma as a delimiter.
     * <p>
     * Examples:
     * Foo, Bar corresponds to ["Foo", "Bar"]
     * "Foo, Bar and Baz", Qux corresponds to ["Foo, Bar and Baz", "Qux"]
     * Foo, "2'10""" corresponds to ["Foo", "2'10\""]
     * Foo,,Bar is invalid. Empty string was not quoted
     *
     * @param input  The string to split
     *
     * @return list of tokens. The size of this list is fixed
     *
     * @throws IllegalArgumentException when the input has empty items
     */
    public static List<String> split(final String input) throws IllegalArgumentException {
        try {
            final String[] raw = rawReader.readValue(input);
            for (final String item : raw) {
                if (item.trim().isEmpty()) {
                    throw new IllegalArgumentException(String.format(PARSING_FAILURE_UNQUOTED_VALUES_FORMAT, input));
                }
            }

            return Arrays.asList(defaultReader.readValue(input));
        } catch (final IOException exception) {
            throw new IllegalArgumentException(String.format(PARSING_FAILURE, input), exception);
        }
    }
}
