/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124;

/**
 * Utils that supports better codability for handling errors.
 */
public class ErrorHandlingUtils {

    /**
     * Helper method to throw an exception when a result is expected as a return value (e.g. in a ternary operator).
     * <p>
     * In the example of ternary operator, we cannot have something like
     * <pre>
     *     {@code
     *     return flag != null ? flag : new IllegalArgumentException( ... );
     *     }
     * </pre>
     * With this helper method, however, we can throw exception is this ternary operator like this
     * <pre>
     *     {@code
     *     return flag != null ? flag : ErrorHandlingUtils.insteadThrowRuntime(new IllegalArgumentException( ... ));
     *     }
     * </pre>
     *
     * @param exception  The exception to be thrown
     * @param <T>  The type of the exception being returned
     *
     * @return is only used for type interface. No object is actually ever returned. An exception is always being thrown
     * instead
     */
    public static <T> T insteadThrowRuntime(final RuntimeException exception) {
        throw exception;
    }
}
