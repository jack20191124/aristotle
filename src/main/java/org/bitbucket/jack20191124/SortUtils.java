/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124;

import org.bitbucket.jack20191124.annotation.NotNull;

import net.jcip.annotations.Immutable;
import net.jcip.annotations.ThreadSafe;

import java.util.Comparator;
import java.util.Objects;

/**
 * Assorted methods related to sorting objects.
 */
@Immutable
@ThreadSafe
public final class SortUtils {

    /**
     * Constructor.
     * <p>
     * Suppress default constructor for noninstantiability.
     */
    private SortUtils() {
        throw new AssertionError();
    }

    /**
     * Compares two objects according to their natural ordering and returns whether the former is smaller than then
     * latter.
     *
     * @param first  The former object to be compared
     * @param second  The latter compared object
     *
     * @return {@code true} if the {@code first} is less than the {@code second} according to their natural ordering
     * or {@code false} otherwise
     *
     * @throws NullPointerException if objects to be compared have {@code null} values
     */
    public static boolean firstIsSmaller(final Object first, final Object second) {
        return compare(first, second) < 0;
    }

    /**
     * Compares two objects using a specified {@link Comparator} and returns whether the former is smaller than then
     * latter.
     *
     * @param first  The former object to be compared
     * @param second  The latter compared object
     * @param comparator  The objects comparator
     *
     * @param <T>  The type of the objects being compared
     *
     * @return {@code true} if the {@code first} is less than the {@code second} using the provided {@link Comparator}
     * or {@code false} otherwise
     *
     * @throws NullPointerException if objects to be compared have {@code null} values or the {@code comparator} is
     * {@code null}
     */
    @SuppressWarnings("MultipleStringLiterals")
    public static <T> boolean firstIsSmaller(
            final Object first,
            final Object second,
            final Comparator<? super T> comparator
    ) {
        return compare(first, second, Objects.requireNonNull(comparator, "comparator")) < 0;
    }

    /**
     * Compares two objects according to their natural ordering and returns whether the former is larger than then
     * latter.
     *
     * @param first  The former object to be compared
     * @param second  The latter compared object
     *
     * @return {@code true} if the {@code first} is greater than the {@code second} according to their natural ordering
     * or {@code false} otherwise
     *
     * @throws NullPointerException if objects to be compared have {@code null} values
     */
    public static boolean firstIsLarger(final Object first, final Object second) {
        return compare(first, second) > 0;
    }

    /**
     * Compares two objects using a specified {@link Comparator} and returns whether the former is larger than then
     * latter.
     *
     * @param first  The former object to be compared
     * @param second  The latter compared object
     * @param comparator  The objects comparator
     *
     * @param <T>  The type of the objects being compared
     *
     * @return {@code true} if the {@code first} is greater than the {@code second} using the provided
     * {@link Comparator} or {@code false} otherwise
     *
     * @throws NullPointerException if objects to be compared have {@code null} values or the {@code comparator} is
     * {@code null}
     */
    @SuppressWarnings("MultipleStringLiterals")
    public static <T> boolean firstIsLarger(
            final Object first,
            final Object second,
            final Comparator<? super T> comparator
    ) {
        return compare(first, second, Objects.requireNonNull(comparator, "comparator")) > 0;
    }

    /**
     * Returns whether two objects are equal according to their natural ordering.
     *
     * @param first  One object to be compared
     * @param second  The other compared object
     *
     * @return {@code true} if the two objects are equal or {@code false} otherwise
     *
     * @throws NullPointerException if objects to be compared have {@code null} values
     */
    public static boolean twoAreEqual(final Object first, final Object second) {
        return compare(first, second) == 0;
    }

    /**
     * Returns whether two objects are equal using a specified {@link Comparator}.
     *
     * @param first  One object to be compared
     * @param second  The other compared object
     * @param comparator  The objects comparator
     *
     * @param <T>  The type of the objects being compared
     *
     * @return {@code true} if the two objects are equal or {@code false} otherwise
     *
     * @throws NullPointerException if objects to be compared have {@code null} values or the {@code comparator} is
     * {@code null}
     */
    @SuppressWarnings("MultipleStringLiterals")
    public static <T> boolean twoAreEqual(
            final Object first,
            final Object second,
            final Comparator<? super T> comparator
    ) {
        return compare(first, second, Objects.requireNonNull(comparator, "comparator")) == 0;
    }

    /**
     * Compares two objects according to their natural ordering.
     *
     * @param first  One object to be compared
     * @param second  The other compared object
     *
     * @return a negative integer, zero, or a positive integer as the {@code first} is less than, equal to, or greater
     * than the {@code second}
     *
     * @throws ClassCastException if two objects are not sub-types of {@link Comparable}
     * @throws NullPointerException if objects to be compared have {@code null} values
     */
    private static int compare(final Object first, final Object second) {
        return compare(first, second, null);
    }

    /**
     * Compares two objects using a specified {@link Comparator} or, when {@link Comparator} is {@code null}, compares
     * the two according to their natural ordering.
     *
     * @param first  One object to be compared
     * @param second  The other compared object
     * @param comparator  An optional objects comparator
     *
     * @param <T>  The type of the objects being compared
     *
     * @return a negative integer, zero, or a positive integer as the {@code first} is less than, equal to, or greater
     * than the {@code second}
     *
     * @throws ClassCastException if two objects are not sub-types of {@link Comparable} when {@link Comparator}
     * is {@code null}
     * @throws NullPointerException if objects to be compared have {@code null} values
     */
    @SuppressWarnings("unchecked")
    private static <T> int compare(
            @NotNull final Object first,
            @NotNull final Object second,
            final Comparator<? super T> comparator
    ) {
        Objects.requireNonNull(first, "first");
        Objects.requireNonNull(second, "second");

        if (comparator == null) {
            return ((Comparable<? super T>) first).compareTo((T) second);
        } else {
            return comparator.compare((T) first, (T) second);
        }
    }
}
