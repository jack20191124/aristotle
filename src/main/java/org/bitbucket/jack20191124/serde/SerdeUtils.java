/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.serde;

import org.bitbucket.jack20191124.annotation.NotNull;

import net.jcip.annotations.Immutable;
import net.jcip.annotations.ThreadSafe;

import java.util.Objects;
import java.util.function.Function;

/**
 * Assorted methods for handling serialization/deserialization abstractions.
 */
@Immutable
@ThreadSafe
public final class SerdeUtils {

    /**
     * Constructor.
     * <p>
     * Suppress default constructor for noninstantiability.
     */
    private SerdeUtils() {
        throw new AssertionError();
    }

    /**
     * Serializes an object using a specified serialization strategy.
     * <p>
     * This method serves as one level of abstraction used by other methods
     * <p>
     * It is the responsibility of the specified serializater to handle the case of {@code null} or special instances
     * of type {@code T}.
     *
     * @param object  The object to be serialized
     * @param serializer  The provided object serializer
     *
     * @param <T>  The type of the serialized object
     *
     * @return a string representation of the {@code object}
     *
     * @throws NullPointerException if {@code serializer} is {@code null}
     */
    public static <T> String serialize(final T object, @NotNull final Function<T, String> serializer) {
        return Objects.requireNonNull(serializer, "serializer").apply(object);
    }

    /**
     * Deserializes an object using a specified deserializatin strategy.
     * <p>
     * This method serves as one level of abstraction used by other methods
     * <p>
     * It is the responsibility of the specified deserialization strategy to handle the case of {@code null}, empty, or
     * special case string.
     *
     * @param serialized  The object in its serialized form
     * @param deserializer  The provided object deserializer
     *
     * @param <T>  The type of the object to deserialize to
     *
     * @return an object represented by the string
     *
     * @throws NullPointerException if {@code deserializer} is {@code null}
     */
    public static <T> T deserialize(final String serialized, @NotNull final Function<String, T> deserializer) {
        return Objects.requireNonNull(deserializer, "deserializer").apply(serialized);
    }
}
