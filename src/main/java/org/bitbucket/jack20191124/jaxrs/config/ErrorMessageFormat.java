/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.jaxrs.config;

import org.bitbucket.jack20191124.time.Granularity;

/**
 * Common message formats for error handling in JUtility.
 */
public enum ErrorMessageFormat implements MessageFormatter {

    /* Hadoop */

    /**
     * {@link org.bitbucket.jack20191124.hadoop.HadoopRemoteFileSupplier}.
     */
    FILE_REMOTE_FETCH_ERROR("Error loading file from remote URI %s to local URI %s"),

    /**
     * {@link org.bitbucket.jack20191124.hadoop.HadoopRemoteFileSupplier}.
     */
    FILESYSTEM_OPEN_ERROR("Error opening Hadoop file system."),

    /* Utils */

    /**
     * {@link org.bitbucket.jack20191124.FileUtils#createParentDirectories(String)}.
     */
    CREATE_DIR_ERROR("Could not create dir '%s'"),

    /**
     * Error while generating MDBM iterator.
     */
    BUILD_MDBM_ITERATOR_ERROR("Error building MDBM iterator"),

    /**
     * Timeout.
     */
    DB_READ_LOCK_ERROR(RepeatingString.DB_TRANSACTION_ERROR, "Unable to read lock %s"),

    /**
     * Timeout.
     */
    DB_WRITE_LOCK_ERROR(RepeatingString.DB_TRANSACTION_ERROR, "Unable to write lock %s"),

    /**
     * Get Left from a Right {@link org.bitbucket.jack20191124.Either}.
     */
    EITHER_ERROR_LEFT_OF_RIGHT(
            RepeatingString.AN_INTERNAL_ERROR_OCCURRED,
            "Attempt to get the Left value of a Right Either: %s"
    ),

    /**
     * Get Right from a Left {@link org.bitbucket.jack20191124.Either}.
     */
    EITHER_ERROR_RIGHT_OF_LEFT(
            RepeatingString.AN_INTERNAL_ERROR_OCCURRED,
            "Attempt to get the Right value of a Left Either: %s"
    ),

    /**
     * Error on calling iterator next.
     */
    FETCHING_NEXT_KEY_EXCEPTION("Exception whle fetching next key."),

    /**
     * Error on get operation by key.
     */
    GET_OPERATION_ERROR("Error retrieving value of key %s"),

    /**
     * Error while replacing key value store.
     */
    KEY_VALUE_STORE_REPLACEMENT_ERROR("Unable to replace %s at %s with %s", "File copy error between %s and %s"),

    /**
     * Error on obtaining a handle from MDBM handle pool.
     */
    MDBM_HANDLE_FETCH_ERROR("Error getting DB handle."),

    /**
     * Enum type has no constant with the specified name.
     */
    NOT_ALTERNATE_KEY_FOR_ENUM("Not an alternate key for %s: %s"),

    /**
     * Error while trying to perform some action on a null key.
     */
    NULL_KEY_OPERATION("Cannot %s null key"),

    /**
     * Error while opening MDBM database.
     */
    OPEN_MDBM_ERROR("Unable to open MDBM DB file with path %s"),

    /**
     * When an interface method does not apply to one type of implementation.
     */
    OPERATION_NOT_SUPPORTED("%s does not support %s"),

    /**
     * Error on put operation.
     */
    PUT_OPERATION_ERROR("Unable to put %s to %s"),

    /**
     * Error message to use when duplicate keys are not desirable.
     */
    TWO_VALUES_OF_THE_SAME_KEY("Values %s and %s are associated with the same key"),

    /**
     * An string that doesn't represent a {@link Granularity}.
     */
    UNKNOWN_GRANULARITY("'%s' is not a valid granularity. Try 'day', 'week', 'month', 'year', or 'all'");

    /**
     * The format string for messaging.
     */
    private final String messageFormat;
    /**
     * The format string for logging.
     */
    private final String loggingFormat;

    /**
     * An error message formatter with different messages for logging and messaging.
     *
     * @param messageFormat  The format string for messaging
     * @param loggingFormat  The format string for logging
     */
    ErrorMessageFormat(final String messageFormat, final String loggingFormat) {
        this.messageFormat = messageFormat;
        this.loggingFormat = loggingFormat;
    }

    /**
     * An error message formatter with the same message for both logging and messaging.
     *
     * @param messageFormat  The format string for both logging and messaging
     */
    ErrorMessageFormat(final String messageFormat) {
        this(messageFormat, messageFormat);
    }

    @Override
    public String getMessageFormat() {
        return messageFormat;
    }

    @Override
    public String getLoggingFormat() {
        return loggingFormat;
    }

    /**
     * A nested class that contains string literals which are used in multiple places.
     */
    private static class RepeatingString {

        /**
         * Usage.
         * <ul>
         *     <li>{@link org.bitbucket.jack20191124.config.ErrorMessageFormat#EITHER_ERROR_LEFT_OF_RIGHT}</li>
         *     <li>{@link org.bitbucket.jack20191124.config.ErrorMessageFormat#EITHER_ERROR_RIGHT_OF_LEFT}</li>
         * </ul>
         */
        private static final String AN_INTERNAL_ERROR_OCCURRED = "An internal error occurred.";

        /**
         * Usage.
         * <ul>
         *     <li>{@link org.bitbucket.jack20191124.config.ErrorMessageFormat#DB_READ_LOCK_ERROR}</li>
         *     <li>{@link org.bitbucket.jack20191124.config.ErrorMessageFormat#DB_WRITE_LOCK_ERROR}</li>
         * </ul>
         */
        private static final String DB_TRANSACTION_ERROR = "A database transaction error occurred";
    }
}
