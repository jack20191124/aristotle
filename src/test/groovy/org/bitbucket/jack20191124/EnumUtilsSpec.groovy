/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124

import static org.bitbucket.jack20191124.jaxrs.config.ErrorMessageFormat.NOT_ALTERNATE_KEY_FOR_ENUM

import spock.lang.Specification
import spock.lang.Unroll

class EnumUtilsSpec extends Specification {
    final static enum TestingEnum {
        DAY,
        GROUP_BY,
        FIELD_ACCESS,
        ONE,
        TWO,
        THIS_IS_A_TEST, // tests short one char word
        A_FIRST,
        LAST_A,
        X_BOTH_Y,
        E_I_E_I_O
    }

    @Unroll
    def "check enumJsonName #a is #b"() {
        expect:
        EnumUtils.enumJsonName(a) == b

        where:
        a                          || b
        TestingEnum.DAY            || "day"
        TestingEnum.GROUP_BY       || "groupBy"
        TestingEnum.FIELD_ACCESS   || "fieldAccess"
        TestingEnum.THIS_IS_A_TEST || "thisIsATest"
        TestingEnum.A_FIRST        || "aFirst"
        TestingEnum.LAST_A         || "lastA"
        TestingEnum.X_BOTH_Y       || "xBothY"
        TestingEnum.E_I_E_I_O      || "eIEIO"
    }

    def "check forKey"() {
        given:
        Map<String, TestingEnum> mapping = ["1": TestingEnum.ONE, "2": TestingEnum.TWO]

        expect:
        TestingEnum.ONE == EnumUtils.forKey("1", mapping, TestingEnum.class)
        TestingEnum.TWO == EnumUtils.forKey("2", mapping, TestingEnum.class)
    }

    def "check missing forKey"() {
        given:
        Map<String, TestingEnum> mapping = ["1": TestingEnum.ONE, "2": TestingEnum.TWO]

        when:
        EnumUtils.forKey("THIS_IS_A_TEST", mapping, TestingEnum.class)

        then:
        IllegalArgumentException exception = thrown()
        exception.message == NOT_ALTERNATE_KEY_FOR_ENUM.format(TestingEnum.class.toString(), "THIS_IS_A_TEST")
    }
}
