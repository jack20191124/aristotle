/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124

import static org.bitbucket.jack20191124.jaxrs.config.ErrorMessageFormat.TWO_VALUES_OF_THE_SAME_KEY

import spock.lang.Specification

import java.util.function.Predicate
import java.util.stream.Stream

class StreamUtilsSpec extends Specification {
    def "When duplicate keys merge, IllegalStateException is thrown"() {
        when: "a key, which is already associates with a value, is inserted to a map"
        [dupKey: 1].merge("dupKey", 2, StreamUtils.throwingMerger())

        then: "insert if rejected"
        IllegalStateException exception = thrown()
        exception.message == TWO_VALUES_OF_THE_SAME_KEY.format(1, 2)
    }

    def "Stream collected to unmodifiable set cannot be modified and element first-in ordering is preserved"() {
        given: "a stream collected to an unmodifiable set"
        Set<Integer> set = StreamUtils.toUnmodifiableSet(Stream.of(1, 2, 3))
        Iterator<Integer> iterator = set.iterator()

        expect: "elements are pulled out in-order"
        iterator.next() == 1
        iterator.next() == 2
        iterator.next() == 3
        !iterator.hasNext()

        when: "we try to modify by adding an element to the set"
        set.add(4)

        then: "it is rejected"
        thrown(UnsupportedOperationException)
    }

    def "Negating a Predicate reverses test result"() {
        given: "a predicate whose test returns true"
        Predicate<Object> predicate = Mock(Predicate) {test() >> true}

        when: "we negate that predicate"
        StreamUtils.not(predicate)

        then: "test returns false"
        !predicate.test()
    }

    def "Appending returns set with the appended element"() {
        expect:
        StreamUtils.append([1, 2] as Set, 3) == [1, 2, 3] as Set
    }

    def "Merging returns set containing two sets"() {
        expect:
        StreamUtils.setMerge([1, 2, 3] as Set, [4, 5, 6] as Set) == [1, 2, 3, 4, 5, 6] as Set
    }
}
