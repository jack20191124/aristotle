/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.granularity

import com.fasterxml.jackson.databind.ObjectMapper

import spock.lang.Specification
import spock.lang.Unroll

class SimpleGranularitySpec extends Specification {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()

    @Unroll
    def "#granularity serializes to #exptectedJson with default timezone (UTC)"() {
        expect:
        OBJECT_MAPPER.writeValueAsString(granularity) == exptectedJson

        where:
        granularity                      || exptectedJson
        SimpleGranularity.ALL            || '{"granularity":"all"}'
        SimpleGranularity.NONE           || '{"granularity":"none"}'
        SimpleGranularity.SECOND         || '{"granularity":"second"}'
        SimpleGranularity.MINUTE         || '{"granularity":"minute"}'
        SimpleGranularity.FIFTEEN_MINUTE || '{"granularity":"fifteen_minute"}'
        SimpleGranularity.THIRTY_MINUTE  || '{"granularity":"thirty_minute"}'
        SimpleGranularity.HOUR           || '{"granularity":"hour"}'
        SimpleGranularity.DAY            || '{"granularity":"day"}'
        SimpleGranularity.WEEK           || '{"granularity":"week"}'
        SimpleGranularity.MONTH          || '{"granularity":"month"}'
        SimpleGranularity.QUARTER        || '{"granularity":"quarter"}'
        SimpleGranularity.YEAR           || '{"granularity":"year"}'
    }
}
