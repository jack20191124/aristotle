/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.filter

import com.fasterxml.jackson.databind.ObjectMapper

import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

class SelectorFilterSpec extends Specification {

    static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()

    @Unroll
    def "Constructor throws error when #reason"() {
        when: "constructor is invoked with null parameter(s)"
        new SelectorFilter(dimension, value)

        then: "NullPointerException is thrown"
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == expectedExceptionMessage

        where:
        dimension | value     | reason                              || expectedExceptionMessage
        null      | null      | "both dimension and value are null" || "dimension"
        null      | "someVal" | "only filtering dimension is null"  || "dimension"
        "someDim" | null      | "only filtering value is null"      || "value"
    }

    @Unroll
    def "'#filter.toString()' serializes to '#expectedJson'"() {
        expect:
        OBJECT_MAPPER.writeValueAsString(filter) == expectedJson

        where:
        dimension | value     | extractionFn || expectedJson
        "someDim" | "someVal" | null         || '{"type":"selector","dimension":"someDim","value":"someVal"}'
        //"someDim" | "someVal" | ImmutableTestingExtractionFunction.builder().type("exFnType").build() || '{"type":"selector","dimension":"someDim","value":"someVal","extractionFn":{"type":"exFnType"}}'

        filter = new SelectorFilter(dimension, value, extractionFn)
    }

    @Ignore
    def "toString() matches doc specification"() {
        given:
        SelectorFilter filter = new SelectorFilter("someDim", "someVal")

        expect:
        filter.toString() == "SelectorFilter{type=selector, dimension=someDim, value=someVal, extractionFn=null}"
    }
}
