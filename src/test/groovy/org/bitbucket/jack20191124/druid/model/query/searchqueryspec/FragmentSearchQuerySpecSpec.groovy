/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.druid.model.query.searchqueryspec

import com.fasterxml.jackson.databind.ObjectMapper

import spock.lang.Specification
import spock.lang.Unroll

class FragmentSearchQuerySpecSpec extends Specification {

    static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()

    @Unroll
    def "#caseSensitive spec on '#values' values serializes to '#expectedJson'"() {
        expect:
        OBJECT_MAPPER.writeValueAsString(new FragmentSearchQuerySpec(isCaseSensitive, values)) == expectedJson

        where:
        isCaseSensitive | values                     || expectedJson
        true            | ["fragment1", "fragment2"] || '{"type":"fragment","case_sensitive":true,"values":["fragment1","fragment2"]}'
        false           | ["fragment1", "fragment2"] || '{"type":"fragment","case_sensitive":false,"values":["fragment1","fragment2"]}'
        true            | null                       || '{"type":"fragment","case_sensitive":true}'
        false           | null                       || '{"type":"fragment","case_sensitive":false}'
        null            | ["fragment1", "fragment2"] || '{"type":"fragment","values":["fragment1","fragment2"]}'
        null            | null                       || '{"type":"fragment"}'

        caseSensitive = isCaseSensitive == null ? "Default" : (isCaseSensitive ? "Case sensitive" : "Case insensitive")
    }
}
