/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124

import spock.lang.Specification
import spock.lang.Unroll

class FilterTokenizerSpec extends Specification {

    @Unroll
    def "Good filter values #values parse correctly to #expected"() {
        expect:
        FilterTokenizer.split(values) == expected

        where:
        values                    | expected
        "foo"                     | ["foo"]
        "foo,bar"                 | ["foo", "bar"]
        '"foo, bar and baz",qux' | ["foo, bar and baz", "qux"]
        'foo,"",bar'            | ["foo", "", "bar"]
        '""'                      | [""]
        // Escaping refers to spock here, not the FilterTokenizer
        'foo,"2\'10"""'          | ["foo", '2\'10"']
    }

    @Unroll
    def "Values #values with empty string throw IllegalArgumentException"() {
        when: "we try to split bad values"
        FilterTokenizer.split(values)

        then: "an IllegalArgumentException is thrown with error message"
        IllegalArgumentException exception = thrown()
        exception.message == String.format(FilterTokenizer.PARSING_FAILURE_UNQUOTED_VALUES_FORMAT, values)

        where:
        values      | _
        ",foo"      | _
        "foo,"      | _
        "foo,,bar"  | _
        "foo, ,bar" | _
        '"",'       | _
        ',""'       | _
        ','         | _
        ',,'        | _
        ' '         | _
        '  '        | _
    }

    def "Invalid CSV string generates parsing error"() {
        when: "we try to parse bad value"
        FilterTokenizer.split("")

        then: "an IllegalArgumentException is thrown with error message"
        IllegalArgumentException exception = thrown()
        exception.message == String.format(FilterTokenizer.PARSING_FAILURE, "")
    }
}
