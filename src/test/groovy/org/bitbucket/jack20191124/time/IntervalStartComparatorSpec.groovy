/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.time

import org.joda.time.Interval

import spock.lang.Specification
import spock.lang.Unroll

class IntervalStartComparatorSpec extends Specification {
    IntervalStartComparator intervalStartComparator

    def setup() {
        intervalStartComparator = new IntervalStartComparator()
    }

    @Unroll
    def "#firstInterval is #comparisionDescription #secondInterval"() {
        expect: "Intervals are correctly compared"
        intervalStartComparator.compare(new Interval(firstInterval), new Interval(secondInterval)) == comparisonValue

        where:
        firstInterval | secondInterval | comparisonValue | comparisionDescription
        "2018-01-01/2018-01-05" | "2018-01-07/2018-01-10" | -1 | "less than"
        "2018-01-07/2018-01-10" | "2018-01-01/2018-01-05" | 1  | "greater than"
        "2018-01-01/2018-01-05" | "2018-01-01/2018-01-05" | 0  | "equal to"
    }

    def "A TreeSet of Intervals is sorted in ascending order when using an IntervalStartComparator"() {
        given: "A TreeSet using an IntervalStartComparator"
        TreeSet<Interval> treeSet = new TreeSet<>(intervalStartComparator)

        and: "A list of intervals in the expected (sorted) order"
        List<Interval> intervalSortedList = [
                "2018-01-01/2018-01-05",
                "2018-01-07/2018-01-15",
                "2018-01-18/2018-01-25"
        ].collect {new Interval(it)}

        when: "We add the intervals to the TreeSet in a different order than they should be"
        treeSet.add(intervalSortedList[2])
        treeSet.add(intervalSortedList[1])
        treeSet.add(intervalSortedList[0])

        then: "The TreeSet gives them back in the expected (sorted) order"
        treeSet.asList() == intervalSortedList
    }
}
