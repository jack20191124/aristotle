/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.time

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.Interval
import org.joda.time.Period

import spock.lang.Specification
import spock.lang.Unroll

class IntervalPeriodIteratorSpec extends Specification {
    def setupSpec() {
        DateTimeZone.setDefault(DateTimeZone.UTC)
    }

    @Unroll
    def "boundaryAt() returns Date #expectedInstant with period #period and n #n from interval #interval"() {
        expect:
        boundaryAtCalculationIsCorrect(interval, period, n, expectedInstant)

        where:
        interval    | period | n || expectedInstant
        "2018/2020" | "P1D"  | 1 || "2018-01-02T00:00:00.000Z"
        "2018/2020" | "PT1H" | 1 || "2018-01-01T01:00:00.000Z"
    }

    @Unroll
    def "boundaryAt() is aligned to interval start for #interval with period #period"() {
        expect:
        boundaryAtCalculationIsCorrect(interval, period, n, expectedInstant)

        where:
        interval                              | period | n || expectedInstant
        "2018/2020"                           | "P1W"  | 1 || "2018-01-08T00:00:00.000Z" // it's 01-08 since it started
                                                                                         // with 01-01
        "2018-01-06T01:01:00.000Z/2018-02-06" | "P5D"  | 2 || "2018-01-16T01:01:00.000Z"
    }

    @Unroll
    def "boundaryAt() is perserved relative to grain period #period even with ragged edge with interval #interval"() {
        expect:
        boundaryAtCalculationIsCorrect(interval, period, n, expectedInstant)

        where:
        interval          | period | n || expectedInstant
        "2014-01-31/2020" | "P1M"  | 1 || "2014-02-28T00:00:00.000Z"
        "2014-01-31/2020" | "P1M"  | 2 || "2014-03-31T00:00:00.000Z"
        "2014-01-31/2020" | "P1M"  | 3 || "2014-04-30T00:00:00.000Z"
    }

    @Unroll
    def "#does have next for #interval with #period when at #position"() {
        given: "an IntervalPeriodIterator with a specified interval to be splitted and a period to divide by with a" +
                "adjusted position(starting instance of time of the next slice to be returned)"
        IntervalPeriodIterator intervalPeriodIterator = getIteratorToTest(period, interval)
        intervalPeriodIterator.currentPosition = new DateTime(position)

        expect: "iterator has next"
        intervalPeriodIterator.hasNext() == expected

        where:
        interval    | period | position || expected
        "2018/2020" | "P1Y"  | "2020"   || false
        "2018/2020" | "P1Y"  | "2021"   || false
        "2018/2020" | "P1Y"  | "2019"   || true

        does = expected ? "Does" : "Does not"
    }

    def "Iterator returns hasNext and then finishes"() {
        when: "an IntervalPeriodIterator spanning 2 years with slicing period of 1 year is created"
        IntervalPeriodIterator intervalPeriodIterator = getIteratorToTest("P1Y", "2018/2020")

        then: "the iterator has next"
        intervalPeriodIterator.hasNext()

        then: "the next is the 1st year"
        intervalPeriodIterator.next() == new Interval("2018/2019")

        then: "the iterator still has next"
        intervalPeriodIterator.hasNext()

        then: "the next is the 2nd year"
        intervalPeriodIterator.next() == new Interval("2019/2020")

        then: "the iterator runs out of next"
        !intervalPeriodIterator.hasNext()
    }

    def "Next for uneven intervals return a truncated period"() {
        when: "a IntervalPeriodIterator spanning 1 year and 1 month with slicing period of 1 year"
        IntervalPeriodIterator intervalPeriodIterator = getIteratorToTest("P1Y", "2018/2019-02-01")

        then: "the iterator's next is the 1st entire year"
        intervalPeriodIterator.hasNext()
        intervalPeriodIterator.next() == new Interval("2018/2019")

        then: "the iterator's next next is the remaining 1 month"
        intervalPeriodIterator.hasNext()
        intervalPeriodIterator.next() == new Interval("2019/2019-02-01")

        then: "the iterator runs out of next"
        !intervalPeriodIterator.hasNext()
    }

    def "Next fails if next called when it doesn't have next"() {
        given: "a IntervalPeriodIterator spanning 1 year with slicing period of 1 year and all slices have been " +
                "extracted"
        IntervalPeriodIterator intervalPeriodIterator = getIteratorToTest("P1Y", "2018/2019")
        intervalPeriodIterator.next()

        expect: "the iterator runs out of next"
        !intervalPeriodIterator.hasNext()

        when: "trying to get next"
        intervalPeriodIterator.next()

        then: "an error is thrown because we don't have next anymore"
        thrown(NoSuchElementException)
    }

    /**
     * Asserts that boundaryAt(n) returns correct output.
     *
     * @param interval  The interval which is to be divided
     * @param period  The period to divide the interval by
     * @param n  The number of periods from the start of the interval
     * @param expectedInstant  The expected return value(in String) of boundaryAt()
     */
    void boundaryAtCalculationIsCorrect(String interval, String period, int n, String expectedInstant) {
        IntervalPeriodIterator intervalPeriodIterator = new IntervalPeriodIterator(
                new Period(period),
                new Interval(interval)
        )
        assert intervalPeriodIterator.boundaryAt(n) == new DateTime(expectedInstant)
    }

    /**
     * Returns a testing IntervalPeriodIterator with specified period and interval.
     *
     * @param period  The period to divide the interval by
     * @param interval  The interval which is to be divided
     *
     * @return the testing IntervalPeriodIterator with the specified period and interval
     */
    IntervalPeriodIterator getIteratorToTest(String period, String interval) {
        new IntervalPeriodIterator(new Period(period), new Interval(interval))
    }
}
