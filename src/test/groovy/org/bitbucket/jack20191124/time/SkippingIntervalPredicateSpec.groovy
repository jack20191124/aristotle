package org.bitbucket.jack20191124.time

import org.joda.time.DateTime
import org.joda.time.Interval

import spock.lang.Specification
import spock.lang.Unroll

class SkippingIntervalPredicateSpec extends Specification {
    @Unroll
    def "skipAhead #comment with #target"() {
        given: "a SkippingIntervalPredicate instance with IS_SUBINTERVAL as a sub-predicate"
        SimplifiedIntervalList.SkippingIntervalPredicate skippingIntervalPredicate =
                new SimplifiedIntervalList.SkippingIntervalPredicate(
                        buildSimplifiedIntervalList(["2015/2017", "2018/2020"]),
                        SimplifiedIntervalList.IsSubinterval.IS_SUBINTERVAL,
                        false
                )

        when: "we call skipAhead(DateTime) on a DateTime on the SkippingIntervalPredicate instance"
        skippingIntervalPredicate.skipAhead(new DateTime(target))

        then: "the SkippingIntervalPredicate instance skips to the correct next"
        skippingIntervalPredicate.activeInterval == (
                expectedNextString == null ? null : new Interval(expectedNextString)
        )
        skippingIntervalPredicate.supply.hasNext() == expectedHasNext

        where:
        target       | expectedNextString | expectedHasNext | comment
        "2021-01-01" | null               | false           | "Date beyond end exhausts the iterator"
        "2011-01-01" | "2015/2017"        | true            | "Date before start doesn't change the iterator"
        "2015-01-01" | "2015/2017"        | true            | "Date doesn't advance on inclusive start"
        "2019-01-01" | "2018/2020"        | false           | "Date doesn't advance beyond interval on interior date"
        "2017-01-01" | "2018/2020"        | false           | "Date advances on exclusive end"
    }

    @Unroll
    def "Subinterval test returns value of #result for #testInterval"() {
        given: "an IsSubinterval instance with some supplied intervals"
        SimplifiedIntervalList.IsSubinterval isSubinterval = new SimplifiedIntervalList.IsSubinterval(
                buildSimplifiedIntervalList(["2015/2017", "2018/2020", "2011-01-01T14:00:00/2012-02-01"])
        )

        expect:
        isSubinterval.test(new Interval(testInterval)) == result

        where:
        testInterval               | result
        "2015/2016"                | true
        "2015-01-01/2016-12-31"    | true
        "2014/2015"                | false
        "2014/2016"                | false
        "2011-01-01T14:00:00/2012" | true
    }

    /**
     * Returns an instance of SimplifiedIntervalList from a collection of Interval Strings.
     *
     * @param intervals  The collection of Interval Strings
     *
     * @return the instance of SimplifiedIntervalList
     */
    SimplifiedIntervalList buildSimplifiedIntervalList(Collection<String> intervals) {
        intervals.collect { new Interval(it) } as SimplifiedIntervalList
    }
}
