/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.serde

import spock.lang.Specification

class SerdeUtilsSpec extends Specification {

    @SuppressWarnings(["GroovyAccessibility", "GroovyResultOfObjectAllocationIgnored"])
    def "Class is non-instantiable"() {
        when: "instantiating this util class"
        new SerdeUtils()

        then: "instantiation is not allowed"
        thrown(AssertionError)
    }

    def "Object can be serialized with specified serializer"() {
        expect:
        SerdeUtils.serialize(Integer.valueOf(1), Integer.&toString) == "1"
    }

    def "Serializer cannot be null"() {
        when: "a null serializer is used"
        SerdeUtils.serialize(Integer.valueOf(1), null)

        then: "an error is thrown"
        Exception exception = thrown(NullPointerException)
        exception.message == "serializer"
    }

    def "String can be deserialized with specified deserializer"() {
        expect:
        SerdeUtils.deserialize("1", Integer.&valueOf) == 1
    }

    def "Deserializer cannot be null"() {
        when: "a null deserializer is used"
        SerdeUtils.deserialize("", null)

        then: "an error is thrown"
        Exception exception = thrown(NullPointerException)
        exception.message == "deserializer"
    }
}
