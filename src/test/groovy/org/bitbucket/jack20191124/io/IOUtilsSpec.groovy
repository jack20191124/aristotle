/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.io

import spock.lang.IgnoreIf
import spock.lang.Specification

/**
 * This spec is disabled due to a Windows bug that fails tests in this spec
 */
@IgnoreIf({System.getProperty("os.name").toLowerCase().contains("windows")})
class IOUtilsSpec extends Specification {

    final static OutputStream DESTINATION = new ByteArrayOutputStream()
    final static PrintStream PRINTER = new PrintStream(DESTINATION)
    static final String TEST_FILE_PATH = getResourceFile("textfile.txt").getAbsolutePath()
    static final InputStream TEST_FILE_CONTENT_STREAM = new FileInputStream(TEST_FILE_PATH)
    static final String EXPECTED_FILE_CONTENT = """
        foo
        中文
        한국어
        """.replaceFirst("\n","").stripMargin().stripIndent()

    @SuppressWarnings(["GroovyAccessibility", "GroovyResultOfObjectAllocationIgnored"])
    def "Class is non-instantiable"() {
        when: "instantiating this util class"
        new IOUtils()

        then: "instantiation is not allowed"
        thrown(AssertionError)
    }

    def "File content loads in memory as a single string"() {
        expect:
        matchIgnoringNewLineChar(IOUtils.readFileAsString(TEST_FILE_PATH), EXPECTED_FILE_CONTENT)
    }

    def "Loading non-existing file generates error"() {
        when: "loading file that does not exists"
        IOUtils.readFileAsString("foo")

        then: "the path is treated as illegal"
        Exception exception = thrown(IllegalArgumentException)
        exception.message == "Error on reading 'foo'"
    }

    def "Loading file content does not accept null path"() {
        when: "file path is null"
        IOUtils.readFileAsString(null)

        then: "the null path is detected"
        Exception exception = thrown(NullPointerException)
        exception.message == "path"
    }

    def "Error stream get printed to std err output"() {
        setup: "error stream will be printed to a specified location"
        System.err = PRINTER

        when: "printing error stream"
        IOUtils.printErrorStream(TEST_FILE_CONTENT_STREAM)

        then: "the specified location received the error stream"
        matchIgnoringNewLineChar(DESTINATION.toString(), EXPECTED_FILE_CONTENT)
    }

    def "Cannot print null error stream"() {
        when: "printing a null error stream"
        IOUtils.printErrorStream(null)

        then: "null error stream is detected"
        Exception exception = thrown(NullPointerException)
        exception.message == "in"
    }

    def "Stream gets printed to specified destination"() {
        setup: "stream will be printed to a specified location"
        System.out = PRINTER

        when: "printing stream"
        IOUtils.printStream(System.out, TEST_FILE_CONTENT_STREAM)

        then: "the specified location received the stream"
        matchIgnoringNewLineChar(DESTINATION.toString(), EXPECTED_FILE_CONTENT)
    }

    def "Cannot print null stream"() {
        when: "printing a null stream"
        IOUtils.printStream(System.out, null)

        then: "null stream is detected"
        Exception exception = thrown(NullPointerException)
        exception.message == "in"
    }

    def "Cannot print with a null printer"() {
        when: "printing with a null printer"
        IOUtils.printStream(null, TEST_FILE_CONTENT_STREAM)

        then: "the null printer is detected"
        Exception exception = thrown(NullPointerException)
        exception.message == "printer"
    }

    /**
     * Returns an object wrapping a specified file under "src/test/resources/".
     *
     * @param relativePathUnderResource  The relative path of the file, i.e. the part after "src/test/resources/"
     *
     * @return a resource file for testing
     */
    static File getResourceFile(String relativePathUnderResource) {
        return new File("src/test/resources/${relativePathUnderResource}")
    }

    /**
     * Compares two strings without taking the line breaks(either UNIX or Windows) into account.
     *
     * @param actual  The test string to be verified
     * @param expected  The correct string in the test result
     *
     * @return {@code true} if two strings are equal in terms of contents
     */
    static boolean matchIgnoringNewLineChar(String actual, String expected) {
        return normalizeLineEnds(actual) == normalizeLineEnds(expected)
    }

    /**
     * Normalize UNIX and Windows line breaks and unifies both to the UNIX standard.
     *
     * @param toBeNormalized  The string (can be multi-lined) whose line breaks are to be transformed
     *
     * @return a string (or multi-lined) with UNIX line breaks only
     */
    static String normalizeLineEnds(String toBeNormalized) {
        return toBeNormalized.replaceAll("\r\n", "\n").replace('\r', '\n');
    }
}
