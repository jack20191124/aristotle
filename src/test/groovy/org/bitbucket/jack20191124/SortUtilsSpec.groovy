/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124


import spock.lang.Specification
import spock.lang.Unroll

class SortUtilsSpec extends Specification {

    static final List<String> THREE_ELEMENT_LIST = Arrays.asList("foo", "bar", "bat")
    static final List<String> SINGLE_ELEMENT_LIST = Arrays.asList("baz")
    static final List<String> SINGLETON_LIST = Collections.singletonList("foo")
    static final Comparator<List<String>> LIST_SIZE_COMPARATOR = new Comparator<List<String>>() {
        @Override
        int compare(final List<String> first, final List<String> second) {
            return Integer.compare(first.size(), second.size())
        }
    }

    @SuppressWarnings(["GroovyAccessibility", "GroovyResultOfObjectAllocationIgnored"])
    def "Util is not instantiable"() {
        when: "trying to create an object of type SortUtils"
        new SortUtils()

        then: "an error is thrown"
        thrown(AssertionError)
    }

    @Unroll
    def "When comparing the first=#first and the second=#second according to natural ordering, the first #is smaller"() {
        expect: "return true iff the first is strictly smaller than the second"
        SortUtils.firstIsSmaller(first, second) == expected

        where:
        first | second || expected
        1     | 2      || true
        1     | 1      || false
        2     | 1      || false

        is = expected ? "is" : "is not"
    }

    def "When testing whether the first is smaller according to natural ordering, objects to be compared cannot be null"() {
        when: "the first object under test is null"
        SortUtils.firstIsSmaller(null, 2)

        then: "NPE is thrown"
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == "first"

        when: "the second object under test is null"
        SortUtils.firstIsSmaller(1, null)

        then: "NPE is also thrown"
        exception = thrown()
        exception instanceof NullPointerException
        exception.message == "second"
    }

    @Unroll
    def "When testing whether the first=#first is smaller than the second=#second based on #criteria, the first #is smaller"() {
        expect: "return true iff the first is strictly smaller than the second"
        SortUtils.firstIsSmaller(first, second, comparator) == expected

        where:
        first               | second              | comparator           | criteria    || expected
        SINGLE_ELEMENT_LIST | THREE_ELEMENT_LIST  | LIST_SIZE_COMPARATOR | "list size" || true
        SINGLE_ELEMENT_LIST | SINGLETON_LIST      | LIST_SIZE_COMPARATOR | "list size" || false
        THREE_ELEMENT_LIST  | SINGLE_ELEMENT_LIST | LIST_SIZE_COMPARATOR | "list size" || false

        is = expected ? "is" : "is not"
    }

    def "When testing whether the first is smaller than the second using comparator, comparator must not be null"() {
        when: "provided comparator is null"
        SortUtils.firstIsSmaller(THREE_ELEMENT_LIST, SINGLE_ELEMENT_LIST, null)

        then: "NPE is thrown"
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == "comparator"
    }

    def "When testing whether the first is smaller than the second using comparator, objects to be compared cannot be null"() {
        when: "the first object under test is null"
        SortUtils.firstIsSmaller(null, SINGLE_ELEMENT_LIST, LIST_SIZE_COMPARATOR)

        then: "NPE is thrown"
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == "first"

        when: "the second object under test is null"
        SortUtils.firstIsSmaller(SINGLE_ELEMENT_LIST, null, LIST_SIZE_COMPARATOR)

        then: "NPE is also thrown"
        exception = thrown()
        exception instanceof NullPointerException
        exception.message == "second"
    }

    @Unroll
    def "When comparing the first=#first and the second=#second according to natural ordering, the first #is larger"() {
        expect: "return true iff the first is strictly larger than the second"
        SortUtils.firstIsLarger(first, second) == expected

        where:
        first | second || expected
        1     | 2      || false
        1     | 1      || false
        2     | 1      || true

        is = expected ? "is" : "is not"
    }

    def "When testing whether the first is larger according to natural ordering, objects to be compared cannot be null"() {
        when: "the first object under test is null"
        SortUtils.firstIsLarger(null, 2)

        then: "NPE is thrown"
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == "first"

        when: "the second object under test is null"
        SortUtils.firstIsLarger(1, null)

        then: "NPE is also thrown"
        exception = thrown()
        exception instanceof NullPointerException
        exception.message == "second"
    }

    @Unroll
    def "When testing whether the first=#first is larger than the second=#second based on #criteria, the first #is larger"() {
        expect: "return true iff the first is strictly larger than the second"
        SortUtils.firstIsLarger(first, second, comparator) == expected

        where:
        first               | second              | comparator           | criteria    || expected
        SINGLE_ELEMENT_LIST | THREE_ELEMENT_LIST  | LIST_SIZE_COMPARATOR | "list size" || false
        SINGLE_ELEMENT_LIST | SINGLETON_LIST      | LIST_SIZE_COMPARATOR | "list size" || false
        THREE_ELEMENT_LIST  | SINGLE_ELEMENT_LIST | LIST_SIZE_COMPARATOR | "list size" || true

        is = expected ? "is" : "is not"
    }

    def "When testing whether the first is larger than the second using comparator, comparator must not be null"() {
        when: "provided comparator is null"
        SortUtils.firstIsLarger(THREE_ELEMENT_LIST, SINGLE_ELEMENT_LIST, null)

        then: "NPE is thrown"
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == "comparator"
    }

    def "When testing whether the first is larger than the second using comparator, objects to be compared cannot be null"() {
        when: "the first object under test is null"
        SortUtils.firstIsLarger(null, SINGLE_ELEMENT_LIST, LIST_SIZE_COMPARATOR)

        then: "NPE is thrown"
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == "first"

        when: "the second object under test is null"
        SortUtils.firstIsLarger(SINGLE_ELEMENT_LIST, null, LIST_SIZE_COMPARATOR)

        then: "NPE is also thrown"
        exception = thrown()
        exception instanceof NullPointerException
        exception.message == "second"
    }

    @Unroll
    def "When testing equality according to natural ordering, #first and #second #are equal"() {
        expect: "comparing equal objects returns true and false otherwise"
        SortUtils.twoAreEqual(first, second) == expected

        where:
        first | second || expected
        1     | 2      || false
        1     | 1      || true

        are = expected ? "are" : "are not"
    }

    def "When testing equality according to natural ordering, objects to be compared cannot be null"() {
        when: "the first object under test is null"
        SortUtils.twoAreEqual(null, 2)

        then: "NPE is thrown"
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == "first"

        when: "the second object under test is null"
        SortUtils.twoAreEqual(1, null)

        then: "NPE is also thrown"
        exception = thrown()
        exception instanceof NullPointerException
        exception.message == "second"
    }

    @Unroll
    def "When testing equality based on #criteria, #first and #second #are equal"() {
        expect: "comparing equal objects returns true and false otherwise"
        SortUtils.twoAreEqual(first, second, comparator) == expected

        where:
        first               | second             | comparator           | criteria    || expected
        SINGLE_ELEMENT_LIST | THREE_ELEMENT_LIST | LIST_SIZE_COMPARATOR | "list size" || false
        SINGLE_ELEMENT_LIST | SINGLETON_LIST     | LIST_SIZE_COMPARATOR | "list size" || true

        are = expected ? "are" : "are not"
    }

    def "When testing equality using comparator, comparator must not be null"() {
        when: "provided comparator is null"
        SortUtils.twoAreEqual(THREE_ELEMENT_LIST, SINGLE_ELEMENT_LIST, null)

        then: "NPE is thrown"
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == "comparator"
    }

    def "When testing equality using comparator, objects to be compared cannot be null"() {
        when: "the first object under test is null"
        SortUtils.twoAreEqual(null, SINGLE_ELEMENT_LIST, LIST_SIZE_COMPARATOR)

        then: "NPE is thrown"
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == "first"

        when: "the second object under test is null"
        SortUtils.twoAreEqual(SINGLE_ELEMENT_LIST, null, LIST_SIZE_COMPARATOR)

        then: "NPE is also thrown"
        exception = thrown()
        exception instanceof NullPointerException
        exception.message == "second"
    }

    @Unroll
    @SuppressWarnings(["GroovyAccessibility"])
    def "Comparing objects with natural ordering returns #expected if first is #first and second is #second"() {
        expect: "comparison gives the correct result"
        SortUtils.compare(first, second) == expected

        where:
        first | second || expected
        1     | 2      || -1
        2     | 1      || 1
        1     | 1      || 0
    }

    @SuppressWarnings(["GroovyAccessibility"])
    def "Comparing null without comparator is not allowed"() {
        when:
        SortUtils.compare(null, THREE_ELEMENT_LIST)

        then:
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == "first"

        when:
        SortUtils.compare(THREE_ELEMENT_LIST, null)

        then:
        exception = thrown()
        exception instanceof NullPointerException
        exception.message == "second"
    }

    @SuppressWarnings(["GroovyAccessibility"])
    def "Comparing objects with no natural ordering without comparator throws error"() {
        when: "comparing list objects by themselves"
        SortUtils.compare(THREE_ELEMENT_LIST, SINGLE_ELEMENT_LIST)

        then: "error is thrown because list are not comparable"
        thrown(ClassCastException)
    }

    @Unroll
    @SuppressWarnings(["GroovyAccessibility"])
    def "When comparing based on #criteria, compare returns #expected if first is #first and second is #second"() {
        expect: "comparison gives the correct result"
        SortUtils.compare(first, second, comparator) == expected

        where:
        first               | second              | comparator           | criteria    || expected
        THREE_ELEMENT_LIST  | SINGLE_ELEMENT_LIST | LIST_SIZE_COMPARATOR | "list size" || 1
        SINGLE_ELEMENT_LIST | THREE_ELEMENT_LIST  | LIST_SIZE_COMPARATOR | "list size" || -1
        THREE_ELEMENT_LIST  | THREE_ELEMENT_LIST  | LIST_SIZE_COMPARATOR | "list size" || 0
    }

    @Unroll
    @SuppressWarnings(["GroovyAccessibility"])
    def "When comparator is not provided, compare returns #expected if first is #first and second is #second"() {
        expect: "comparison gives the correct result"
        SortUtils.compare(first, second, null) == expected

        where:
        first | second || expected
        1     | 2      || -1
        2     | 1      || 1
        1     | 1      || 0
    }

    @SuppressWarnings(["GroovyAccessibility"])
    def "Comparing null using comparator is not allowed"() {
        when: "one of the compared object is null"
        SortUtils.compare(null, THREE_ELEMENT_LIST, LIST_SIZE_COMPARATOR)

        then: "error is thrown"
        Exception exception = thrown()
        exception instanceof NullPointerException
        exception.message == "first"

        when: "the other compared object is null"
        SortUtils.compare(THREE_ELEMENT_LIST, null, LIST_SIZE_COMPARATOR)

        then: "error is also thrown"
        exception = thrown()
        exception instanceof NullPointerException
        exception.message == "second"
    }

    @SuppressWarnings(["GroovyAccessibility"])
    def "When comparator is provided, objects are compared using the comparator, not their natural ordering"() {
        given: "an object that is to be invoked in the happy path"
        Comparator<List<String>> comparator = Mock(Comparator)

        when: "comparing using provided comparator"
        SortUtils.compare(THREE_ELEMENT_LIST, SINGLE_ELEMENT_LIST, comparator)

        then: "the object is invoked, meaning the happy path is executed"
        1 * comparator.compare(THREE_ELEMENT_LIST, SINGLE_ELEMENT_LIST)
    }

    @SuppressWarnings(["GroovyAccessibility"])
    def "Without comparator, comparing non-comparable objects without comparator throws an error"() {
        when: "comparing list objects by themselves"
        SortUtils.compare(THREE_ELEMENT_LIST, SINGLE_ELEMENT_LIST, null)

        then: "error is thrown because list are not comparable"
        thrown(ClassCastException)
    }
}
