/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124.jaxrs.test

import org.eclipse.jetty.server.Server
import org.glassfish.jersey.server.ResourceConfig

import io.restassured.RestAssured
import spock.lang.Specification

import javax.inject.Inject
import javax.ws.rs.ApplicationPath

class JettyServerFactorySpec extends Specification {

    @ApplicationPath("v1")
    class TestResourceConfig extends ResourceConfig {

        @Inject
        TestResourceConfig() {
            packages("org.bitbucket.jack20191124.jaxrs.test.resource")
        }
    }

    def "Factory produces Jsersey-Jetty applications"() {
        setup:
        Server server = JettyServerFactory.newInstance(8080, "/v1/*", new TestResourceConfig())
        server.start()

        expect:
        RestAssured
                .when()
                .get("/v1/example/test") // RestAssured assumes 8080 as default port
                .then()
                .statusCode(200)

        RestAssured
                .when()
                .get("/v1/example/test").asString() == "SUCCESS"

        cleanup:
        server.stop()
    }
}
