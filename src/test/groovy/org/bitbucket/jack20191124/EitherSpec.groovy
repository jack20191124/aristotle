/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124

import spock.lang.Specification
import spock.lang.Unroll

class EitherSpec extends Specification {
    @Unroll
    def "A #leftRight Either<String, Integer> wrapping '#value' can be unwrapped into '#value'"() {
        expect: "We can unwrap an Either into the value it wraps"
        unwrap(wrap(value)) == value

        where:
        leftRight | wrap               | unwrap     || value
        "left"    | {Either.left(it)}  | {it.left}  || "String"
        "right"   | {Either.right(it)} | {it.right} || 5
    }

    @Unroll
    def "A #leftRight Either<String, Integer> #isOrIsNot left"() {
        expect:
        wrap(value).isLeft() == expectedLeftOrNot

        where:
        leftRight | wrap               | isOrIsNot | value    || expectedLeftOrNot
        "left"    | {Either.left(it)}  | "is"      | "string" || true
        "right"   | {Either.right(it)} | "is not"  | 5        || false
    }

    @Unroll
    def "A #leftRight Either<String, Integer> #isOrIsNot right"() {
        expect:
        wrap(value).isRight() == expectedRightOrNot

        where:
        leftRight | wrap               | isOrIsNot | value    || expectedRightOrNot
        "right"   | {Either.right(it)} | "is"      | "string" || true
        "left"    | {Either.left(it)}  | "is not"  | 5        || false
    }

    @Unroll
    def "Attempting to get the #rightLeftValue from a #leftRight Either throws UnsupportedOperationException"() {
        when:
        invalidUnwrap(wrap(value))

        then:
        thrown UnsupportedOperationException

        where:
        rightLeftValue | leftRight | wrap | invalidUnwrap || value
        "right"        | "left"    | {Either.left(it)} | {it.right} || "string"
        "left"         | "right"   | {Either.right(it)} | {it.left} || 5
    }
}
