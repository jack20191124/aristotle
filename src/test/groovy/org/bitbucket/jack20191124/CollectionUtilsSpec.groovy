/*
 * Copyright Jiaqi Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.jack20191124

import spock.lang.Specification

class CollectionUtilsSpec extends Specification {

    def "Get by sub-type returns all instances of that sub-type"() {
        given: "a collection of sets which share the same HashSet class"
        Collection set = [["a"] as HashSet, ["b"] as LinkedHashSet]

        expect: "getting by LinkedHashSet returns LinkedHashSet instance"
        CollectionUtils.getSubsetByType(set, LinkedHashSet) == [["b"] as LinkedHashSet] as LinkedHashSet

        and: "getting by HashSet returns both HashSet and LinkedHashSet instances"
        CollectionUtils.getSubsetByType(set, HashSet) == set as LinkedHashSet
    }
}
